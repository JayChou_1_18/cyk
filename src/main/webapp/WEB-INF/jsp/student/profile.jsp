<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<base href="${BaseContext}>">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>学员的个人信息</title>
<%@include file="../include/styles.jsp"%>
</head>
<body>


	<div id="wrapper">
		<jsp:include page="../include/header_stu.jsp"></jsp:include>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header">个人信息</h3>
				</div>
				<!-- /.col-lg-12 -->
			</div>

			<form class="" action="student/update.do">
			<input type="hidden" name="id" value="${student.id}">
					<div class="form-group">
						<label for="phone">手机号</label> <input type="text"
							class="form-control" id="phone" disabled="disabled"
							placeholder="手机号" value="${student.mobile}">
					</div>
					<div class="form-group">
						<label for="name">姓名</label> <input type="text"
							class="form-control" id="name" placeholder="姓名"
							value="${student.name}" name="name">
					</div>
					<div class="form-group">
						<label for="age">年龄</label> <input type="number"
							class="form-control" id="age" placeholder="年龄"
							value="${student.age}" name="age">
					</div>
					<div class="form-group">
						<label for="address">居住地址</label> <input type="text"
							class="form-control" id="address" placeholder="居住地址"
							value="${student.address}" name="address">
					</div>
					<div class="form-group">
						<label>驾照级别</label>
						<p>${student.licenselevel}</p>
					</div>
					<div class="form-group">
						<label for="gender">性别</label>
						<div class="radio">
							<label> <input type="radio" name="optionsRadios"
								id="gender" value="student2" checked="true"> 男
							</label> &nbsp; <label> <input type="radio" name="optionsRadios"
								id="" value="coach"> 女
							</label>

						</div>
					</div>
					<div class="form-group">
						<p>注册时间：2017-06-01</p>
						<p>更新时间：2017-06-01</p>
					</div>

					<button type="submit" class="btn btn-primary">更新信息</button>
			</form>
		</div>

	</div>

	<%@include file="../include/scripts.jsp"%>



</body>
</html>
