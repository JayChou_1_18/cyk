<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<base href="${BaseContext}>">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>抢教练页面</title>
<%@include file="../include/styles.jsp"%>
<style>
img.media-object {
	width: 80px;
	height: 80px;
}

.media a {
	/*padding-left: 15px;*/
	color: black;
}

.media-price {
	white-space: nowrap;
	padding-right: 10px;
	color: red;
}
</style>
</head>
<body>


	<div id="wrapper">
		<jsp:include page="../include/header_stu.jsp"></jsp:include>
		<div id="page-wrapper">
			<div class="row">
				<div class="btn-group btn-group-justified" role="group"
					aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default">智能排序</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default">价格↓</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default">接单量</button>
					</div>
				</div>
			</div>
			<div class="row">
				<form role="form">
					<div class="form-group input-group">
						<input type="text" class="form-control" placeholder="搜索教练信息">
						<span class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<div class="media">
					<a href="student/coachInfo.do">
						<div class="media-left media-middle">

							<img class="media-object" src="static/images/img.jpeg" alt="头像">

						</div>
						<div class="media-body">
							<h4 class="media-heading">老王教练</h4>
							<p>接单量 100</p>
							<p>郑州市经开区</p>
						</div>
						<div class="media-right media-middle">
							<span class="media-price">60元/小时</span>
						</div>
					</a>
				</div>
				<div class="media">
					<a href="student/coachInfo.do">

						<div class="media-left media-middle">
							<img class="media-object" src="static/images/img.jpeg" alt="头像">
						</div>
						<div class="media-body">
							<h4 class="media-heading">老王教练</h4>
							<p>接单量 100</p>
							<p>郑州市经开区</p>
						</div>
						<div class="media-right media-middle">
							<span class="media-price">60元/小时</span>
						</div>
					</a>
				</div>


				<c:forEach items="${coachList}" var="coachs">
				
					<div class="media">
						<a href="student/showCoachMessage.do?id=${coachs.id}" >
							
							<div class="media-left media-middle">
								<img class="media-object" src="static/images/img.jpeg" alt="头像">
							</div>

							<div class="media-body">
								<h4 class="media-heading">${coachs.name}</h4>
								<p></p>
								<p>${coachs.address}</p>
							</div>
							<div class="media-righ media-middle">
								<span class="media-price"></span>
							</div>
						</a>
					</div>
				</c:forEach>

			</div>
			<div class="row text-center">
				<a href="student/showAddCoach.do">加载更多...</a>
			</div>

		</div>

	</div>

	<%@include file="../include/scripts.jsp"%>
<script type="text/javascript">
	function submitFrom(id) {
		$("id").val(id);
		$("#formSubmitCoach").submit();
	}

</script>
</body>
</html>
