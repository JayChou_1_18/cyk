<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<base href="${BaseContext}>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>我的预约</title>
	<%@include file="../include/styles.jsp"%>
</head>
<body>


<div id="wrapper">
	<jsp:include  page="../include/header_stu.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">学员的预约信息</h3>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					<form class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="教练姓名、手机号等信息">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="startTime" id="queryStartTime" placeholder="课程开始时间：yyyy-mm-dd" value="${queryInfo.startTime }">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="endTime" id="queryEndTime" placeholder="课程结束时间：yyyy-mm-dd" value="${queryInfo.endTime }">
						</div>
						<button type="submit" class="btn btn-block btn-default">查询</button>
					</form>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>序号</th>
								<th>教练</th>
								<th>电话</th>
								<th>级别</th>
								<th>时间</th>
								<th>状态</th>
								<th>操作</th>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${appointmentList}" var="appointment" varStatus="status">
								<tr>
									<td>${status.index+1}</td>
									<td>${appointment.coachName}</td>
									<td>${appointment.coachMobile}</td>
									<td>${appointment.licenseLevel}</td>
									<td>${appointment.timeSchedule}</td>
									<%-- <td id="aaa">正常</td>
									<td button class="btn btn-sm btn-warning" onclick="cancel(${appointment.id})">取消</td> --%>
								 	<c:if test="${appointment.orderStatus == 1}">
										<td>正常</td>
										<td button class="btn btn-sm btn-warning" onclick="cancel(${appointment.id})">取消</td>
									</c:if>
									<c:if test="${appointment.orderStatus == -1}">
										<td>已取消</td>
										<td>已取消</td>										
									</c:if> 
								</tr>
							</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../include/scripts.jsp"%>
<script>
	$('#queryStartTime').datetimepicker({format: 'yyyy-mm-dd',minView:'month'});
	$('#queryEndTime').datetimepicker({format: 'yyyy-mm-dd',minView:'month'});
	
	function cancel(id) {
		alert(id)
		
		$.ajax({
			async:false,  //同步 默认true
			type:"POST",
			url:"student/updateMessageState.do",
			/* dataType:"json", */
			data:{"id":id},
			success:function(data){
				if(data=="success"){
					alert("修改成功");
					window.location.reload();
				}else{
					alert("修改失败，请重新提交");
				}
			},
			error:function(data){
				alert("服务器遇到一个内部错误，请联系管理员!");
			}
		});
	}
</script>


</body>
</html>
