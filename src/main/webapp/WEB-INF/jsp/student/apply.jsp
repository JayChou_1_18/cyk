 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>报名页面</title>
	<%@include file="../include/styles.jsp"%>
	<style>

		.passed{
			background-color: lightgrey;
		}
		.passed.schedued{
			background-color:yellow;
		}
		.unpassed.schedued{
			background-color:greenyellow;
		}
		.currentCheck{
			color:darkred;
			font-weight: bold;
		}
		.checkedDate{
			color: red;
			font-weight: bold;
		}
	</style>

<script type="text/javascript">
	function studentApply() {
		$("#applyMoney").val($("#totalPrice").html());
		
		$("#studentFromApply").submit();
	}

</script>
	
</head>
<body>


<div id="wrapper">
	<jsp:include  page="../include/header_stu.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">报名信息</h3>
			</div>
		</div>
		<div class="row">
			<form role="form" style="padding: 16px;" action="student/applySuccess.do" id="studentFromApply">
				<!-- 放一个教练ID 方便插入 -->
				<input type="hidden" name="coachId" value="${addCoach.id}" />
				<div class="form-group">
					<%--<label >教练</label>--%>
					<input type="text" class="form-control" disabled="disabled" placeholder="" value="${addCoach.name}" name="coachName">
				</div>
				<div class="form-group">
					<%--<label >费用</label>--%>
					<input type="text" class="form-control"  disabled="disabled" placeholder="" value="${addCoach.hoursfee}">
					<input type="hidden" value="60" id="unitPrice">
				</div>
				<div class="form-group">
					<label >姓名</label>
					<input type="text" class="form-control" placeholder="" value="张老三" name="studentName">
				</div>
				<div class="form-group">
					<label >联系电话</label>
					<input type="text" class="form-control" placeholder="" value="18600001111" name="studentMobile">
				</div>
				<div class="form-group">
					<label >车型</label>
					<input type="text" class="form-control"  readonly="readonly" placeholder="" value="${addCoach.certificate}" name="licenseLevel">
				</div>
				<div class="form-group">
					<label >备注</label>
					<textarea class="form-control" placeholder="" name="text" >备注信息</textarea>
				</div>
				<div class="form-group">
					<label >课程信息</label>
					<button type="button" class="btn btn-default" id="applyBtn">添加课程</button>
				</div>
				<div id="applyInfo">
				</div>
				<!-- 报名费 -->
				<input type="hidden" name="applyMoney"  id="applyMoney"/>
				<!--时间-->
				<input type="hidden" name="timeSchedule" id="allSchedule" />
				<!-- 课程 -->
				<input type="hidden" name = "addSchedule"  id="addSchedule"/>
			</form>
		</div>

	<div class="row" style="margin: 15px;">
		<a class="btn btn-primary btn-block" onclick="studentApply()"><span id="totalPrice">￥0</span>&nbsp;&nbsp;提交报名</a>
	</div>

</div>
<div class="modal" id="scheduModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<input type="hidden" value="${dateEQ.year}" id="year"/>
	<input type="hidden" value="${dateEQ.month}" id = "month" />
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-center" id="modalTitle">
					<button class="btn btn-default" style="float: left;"><span class="fa fa-angle-left"></span></button>
					<span>课程选择</span>
					<button class="btn btn-default" style="float: right;"><span class="fa fa-angle-right"></span></button>
				</h4>
			</div>
			<div class="modal-body" id="modalContent">
				<div class="row">
					<table class="table table-bordered">
						<thead>
						<tr>
							<td>周日</td>
							<td>周一</td>
							<td>周二</td>
							<td>周三</td>
							<td>周四</td>
							<td>周五</td>
							<td>周六</td>
						</tr>
						</thead>
						<tbody>
						<c:forEach begin="1" end="${dateEQ.day}" var="a" varStatus="i">
							<c:if test="${i.first}">
								<tr>
							</c:if>
							<c:if test="${i.first}">
							<c:forEach begin="1" end="${dateEQ.week-1}">
								<td></td>
							</c:forEach>
							</c:if>
						<c:set value="${dateEQ.monthBeans[a-1].arranged}" var="dateStatus"/>
						<c:set value="${dateEQ.intraday}" var="intraday"/>		
						<td 
							<c:if test="${i.count<=intraday}">
								<c:if test="${dateStatus == 1}">
									class="passed schedued"
								</c:if>
								<c:if test="${dateStatus == 0}">
									class="passed"
								</c:if>
							</c:if>
							
							<c:if test="${i.count>intraday}">
								<c:if test="${dateStatus == 1}">
									class="unpassed schedued"
								</c:if>
								<c:if test="${dateStatus == 0}">
									class="unpassed"
								</c:if>
							</c:if>
						>${a}
							<c:if test="${i.last || (i.count+dateEQ.week-1)%7==0}">
								</tr>
							</c:if>
						</td>
					</c:forEach>
				</tbody>
					</table>
					<div id="classesDiv">

					</div>
					<div id="alertInfo">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				<button type="button" class="btn btn-primary" id="applyClassesBtn">选择</button>
			</div>
		</div>
	</div>
</div>
<%@include file="../include/scripts.jsp"%>
<script>
	$(function () {
		$('#applyBtn').on('click',function () {
			//先清空之前操作的信息
			$('#classesDiv').html('');
			$('#alertInfo').html('');
			$('#scheduModal').modal('show');
		});

		var selDate='';//存储当前点击的日期

		//只对安排了课程的日期响应，未安排课程的，提示没有课程安排即可
		$('.schedued').each(function () {
			//这里根据点击的日期显示当天的
			$(this).on('click',function () {
				//存储当前选择的日期要用的
				selDate =$("#year").val()+"-"+$("#month").val()+"-"+$(this).html();
				var a= $("#allSchedule").val(selDate);  //将当前点击的年月日放入隐藏的input框中
				
				console.log("当前点击了日期："+selDate);
//				$(this).append('<span class="fa fa-check"></span>');

				//遍历已经有这个样式的，然后把它取消掉，每次只能选择一个
				$('td.currentCheck').each(function () {
					$(this).removeClass('currentCheck');
				});
				//如果当前日期已经选择了，不能再选，只能先删除再添加，这样简单点
				if($(this).hasClass('checkedDate')){
					$('#alertInfo').html('今天的课程已经选择，如果要修改请先删除再添加');
					$('#classesDiv').html('');
				}else{
					//给当前点击的td加上样式
					$(this).addClass('currentCheck');
					$('#alertInfo').html('');
					$('#classesDiv').html(
						'<form role="form"><div class="form-group"><div class="checkbox"><label class="checkbox-inline"><input type="checkbox" value="上午">上午(3h)</label>	<label class="checkbox-inline"><input type="checkbox" value="下午">下午(4h)</label>	<label class="checkbox-inline"><input type="checkbox" value="晚上">晚上(2h)</label>	</div>	</div>	</form>'
					);
				}

			});


		});
		//点击选择按钮的时候检查是否选择了课程，如果没有提示，有的话可以添加
		//添加的时候还需要判断重复性的问题
		$('#applyClassesBtn').on('click',function () {
			var cc = $('input:checkbox');
			if (cc.length == 0) {
				$('#alertInfo').html('<div class="alert alert-warning alert-dismissable">'+
					'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
					'请先选择课程</div>');
			} else {
				//循环获取已选择的checkbox
				var period='';
				$(cc).each(function () {
					if($(this).prop('checked')){
						period+='/'+$(this).val();
					}
				});
				$("#addSchedule").val(period);
				if(period.length==0){
					$('#alertInfo').html('<div class="alert alert-warning alert-dismissable">'+
						'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
						'请先选择课程</div>');
				}else{
					//确认选择了添加上样式，遍历所有的比较日期
					$('td.schedued').each(function () {
						var d = $(this).data('date');
						if(d==selDate){
							$(this).addClass("checkedDate");
						}
					});

					$('#applyInfo').append(
						'<div class="alert alert-success alert-dismissable"'+' data-date="'+selDate+'" data-period="'+period+'" >'+
						'<button type="button" class="close" onclick="clearChecked(\''+selDate+'\');">×</button>'+
						selDate+'   '+period+'</div>'
					);
					$('#scheduModal').modal('hide');

					//调用计算方法，计算一共的价格是多少
					calculatePrice();

				}
			}

		});

		//模态框关闭的时候，把已经点击选择的日期样式去掉
		$('#scheduModal').on('hide.bs.modal', function () {
			$('td.schedued').each(function () {
				if($(this).hasClass('currentCheck')){
					$(this).removeClass('currentCheck');
				}
			});
		})


	});


	function clearChecked(da) {
		console.log("清除已选择日期："+da);
		$('td.schedued').each(function () {
			var d = $(this).data('date');
			if(d==da){
				$(this).removeClass("checkedDate");
				//将当前的记录删除
				$('div.alert').each(function () {
					var d = $(this).data('date');
					if(d==da){
						$(this).remove();
						//调用计算方法，计算一共的价格是多少
						calculatePrice();
					}
				});
				return;
			}
		});
	}

	//计算总价格
	function calculatePrice() {
		
		//计算价格的策略就是统计已选择的课程中am/pm/night各有多少，然后按照am3小时，pm4小时，night2小时计算总时间，然后再乘单价
		var ams = 0;
		var pms = 0;
		var nis = 0;
		
		$('div.alert').each(function () {
			var d = $(this).data('period');
			console.log(d);
			if(d!='' && d!=undefined){
				//处理字符串
				var s = d.split('/');
				console.log(s);
				for(var i=0;i<s.length;i++){
					if(s[i]!=''){
						var ii = s[i];
						if(ii.toLowerCase()=='上午'){
							ams++;
						}else if(ii.toLowerCase()=='下午'){
							pms++;
						}else if(ii.toLowerCase()=='晚上'){
							nis++;
						}
					}
				}
			}
		});
		console.log(ams+'/'+pms+'/'+nis);
		//计算价格
		var unitPrice = $('#unitPrice').val();
		var total = (ams*3+pms*4+nis*2)*unitPrice;
		$('#totalPrice').html('￥'+total);
	}

</script>
</body>
</html>
