<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<base href="${BaseContext}>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>教练详情页面</title>
	<%@include file="../include/styles.jsp"%>
	<style>
		img.media-object{
			width: 80px;
			height: 80px;
		}
		.media{
			padding: 15px;
		}
		.media-price{
			white-space: nowrap;
			padding-right: 10px;
			color: red;
		}

		.passed{
			background-color: lightgrey;
		}
		.passed.schedued{
			background-color:yellow;
		}
		.unpassed.schedued{
			background-color:greenyellow;
		}
	</style>
</head>
<body>


<div id="wrapper">
	<input type="hidden" name="id" value="${coach.id}" />
	<jsp:include  page="../include/header_stu.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="media">
				<div class="media-body">
					<h3 class="media-heading">${coach.name}</h3>
					<p>2017-05-25 更新</p>
					<p class="media-price">60元/小时</p>
					<p>接单量 100</p>
					<p>郑州市经开区</p>
				</div>

				<div class="media-right media-middle">
					<img class="media-object" src="static/images/img.jpeg" alt="头像">
				</div>

			</div>
		</div>
		<hr>
		<div class="row">
			<h3>执教课程安排</h3>
			<table class="table table-bordered">
				<thead>
				<tr>
					<td>周日</td>
					<td>周一</td>
					<td>周二</td>
					<td>周三</td>
					<td>周四</td>
					<td>周五</td>
					<td>周六</td>
				</tr>
				</thead>
				<tbody>
				<c:forEach begin="1" end="${dateEQ.day}" var="a" varStatus="i">
					<c:if test="${i.first}">
						<tr>
					</c:if>
					<c:if test="${i.first}">
						<c:forEach begin="1" end="${dateEQ.week-1}">
							<td></td>
						</c:forEach>
					</c:if>
					<c:set value="${dateEQ.monthBeans[a-1].arranged}" var="dateStatus"/>
					<c:set value="${dateEQ.intraday}" var="intraday"/>
					<td
						<c:if test="${i.count<=intraday}">
							<c:if test="${dateStatus == 1}">
								class="passed schedued"  title="课程安排" tabindex="0" role="button" data-placement="top" data-container="body" data-toggle="popover" data-trigger="focus" data-content="课程信息：${dateEQ.monthBeans[a-1].timestatus}"
							</c:if>
							
							<c:if test="${dateStatus == 0}">
								class="passed" title="课程安排" tabindex="0" role="button" data-placement="top" data-container="body" data-toggle="popover" data-trigger="focus" data-content="今天没有课程信息"
							</c:if>
						</c:if>
						
						<c:if test="${i.count>intraday}">
							<c:if test="${dateStatus == 0}">
								class="unpassed" title="课程安排" tabindex="0" role="button" data-placement="top" data-container="body" data-toggle="popover" data-trigger="focus" data-content="今天没有课程信息"
							</c:if>
							
							<c:if test="${dateStatus == 1}">
								class="unpassed schedued"  title="课程安排" tabindex="0" role="button" data-placement="top" data-container="body" data-toggle="popover" data-trigger="focus" data-content="课程信息：${dateEQ.monthBeans[a-1].timestatus}"
							</c:if>
						</c:if>
					>${a}</td>
						<c:if test="${i.last || (i.count+dateEQ.week-1)%7==0}">
							</tr>
						</c:if>
				</c:forEach>
				</tbody>
			</table>
			<table>
				<tr>
					<td style="background-color: lightgrey;">不可操作日期</td>
					<td style="background-color: yellow;">已过期的课程</td>
					<td style="background-color: greenyellow;">已安排的课程</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row" style="margin: 15px;">
		<a class="btn btn-primary btn-block" href="student/apply.do?id=${coach.id}">立即报名</a>

	</div>

</div>
<div class="modal" id="scheduModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modalTitle"></h4>
			</div>
			<div class="modal-body" id="modalContent">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>

<form action="student/submitSchenule.do" id="submitSchedule">
	<input name="month" value="${dateEQ.month}" id="month" type="hidden" />
	<input name="year" value="${dateEQ.year}" id="year"  type="hidden"/>
	<input name="day" value="${dateEQ.day}" id="day" type="hidden"/>
</form>
<%@include file="../include/scripts.jsp"%>
<script>
	$(function () {
		//初始化所有的popover提示框
		$("[data-toggle='popover']").popover();
	});
	
	
	 //未过期时间处理
	$('td.unpassed').each(function(){
		$(this).on('click', function (){
			$("#day").val($(this).html());
			
			if ($(this).hasClass('schedued')){
			
				$("#submitSchedule").submit();
				/*  $.ajax({
					async:false,  //同步 默认true
					type:"POST",
					url:"student/submitSchedule.do",
					data:$("#submitSchedule").serialize,
					success:function(data){
						
					},
					error:function(date){
						alert("服务器遇到一个内部错误，请联系管理员");
					}
				});  */
			}
		});
	}); 
	
	
</script>
</body>
</html>
