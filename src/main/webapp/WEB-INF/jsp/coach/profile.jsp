<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<base href="${BaseContext}>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>教练首页</title>
	<%@include file="../include/styles.jsp"%>
</head>
<body>


<div id="wrapper">
	<jsp:include  page="../include/header_coa.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">个人信息</h3>
			</div>
			<!-- /.col-lg-12 -->
		</div>
			<div class="text-center"><img src="${coach.avatar}" height="80px" width="80px"></div>
			<form  action="coach/updateAll.do"  method="post" enctype="multipart/form-data">
			<input type="hidden" name="id" value="${coach.id}">
				<div class="form-group">
					<label for="phone">手机号</label>
					<input type="text" class="form-control" id="phone" disabled="disabled" placeholder="手机号" value="${coach.mobile}">
				</div>
				<div class="form-group">
					<label for="name">姓名</label>
					<input type="text" class="form-control" id="name" placeholder="姓名" value="${coach.name}" name="name">
				</div>
				<div class="form-group">
					<label for="age">年龄</label>
					<input type="number" class="form-control" id="age" placeholder="年龄" value="${coach.age}" name="age">
				</div>
				<div class="form-group">
					<label for="jage">教龄</label>
					<input type="number" class="form-control" id="jage" placeholder="教龄" value="${coach.teachingage}" name="teachingage">
				</div>
				<div class="form-group">
					<label for="idnum">身份证号</label>
					<input type="text" class="form-control" id="idnum" placeholder="身份证号" value="410411199510230111">
				</div>
				<div class="form-group">
					<label for="address">所在区域</label>
					<input type="text" class="form-control" id="address" placeholder="所在区域" value="郑州市经开区">
				</div>
				<div class="form-group">
					<label for="fee">每小时费用(元)</label>
					<input type="number" class="form-control" id="fee" placeholder="每小时费用" value="50">
				</div>
				<div class="form-group">
					<label for="img">更新照片</label>
					<input type="file" class="form-control" id="imageFile" name="imageFile">
				</div>
				<div class="form-group">
					<label>证书级别</label>
					<p>A1</p>
				</div>
				<div class="form-group">
					<label for="gender">性别</label>
					<div class="radio">
						<label>
							<input type="radio" name="gender" id="man"  value="男"/>
							男
						</label>
						&nbsp;
						<label>
							<input type="radio" name="gender" id="woman" value="女"/>
							女
						</label>

					</div>
				</div>
				<div class="form-group">
					<p>注册时间：2017-06-01</p>
					<p>更新时间：<fmt:formatDate value="${coach.updatetime}" type="both"/></p>
				</div>


				<button type="submit" class="btn btn-primary">更新信息</button>
			</form>
	</div>

</div>

<%@include file="../include/scripts.jsp"%>
<script type="text/javascript">
	$(function() {
		var man=$("#man").val();
		var woman = $("#woman").val();
		var sex='${coach.gender}';
		
		if(man==sex){
			$("#man").attr("checked",true);
		}else{
			$("#woman").attr("checked",true);
		}
		
		
		/* var sex=$("coach.gender")
	
		$("input[name='gender']").each(function(i,obj){
			if($(obj).val()==sex){
				$(obj).attr("checked",true);
			}
		}); */
		
	});

</script>


</body>
</html>
