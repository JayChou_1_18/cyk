<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<base href="${BaseContext}>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>我的预约</title>
	<%@include file="../include/styles.jsp"%>
</head>
<body>


<div id="wrapper">
	<jsp:include  page="../include/header_coa.jsp"></jsp:include>
	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">教练的预约信息</h3>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-heading">
					<form class="form-inline">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="学员名称、手机号等信息">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="startTime" id="queryStartTime" placeholder="课程开始时间：yyyy-mm-dd" value="${queryInfo.startTime }">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="endTime" id="queryEndTime" placeholder="课程结束时间：yyyy-mm-dd" value="${queryInfo.endTime }">
						</div>
						<button type="submit" class="btn btn-block btn-default">查询</button>
					</form>
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">
							<thead>
							<tr>
								<th>序号</th>
								<th>学员</th>
								<th>电话</th>
								<th>级别</th>
								<th>时间</th>
								<th>状态</th>
							</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>张三</td>
									<td>18600001111</td>
									<td>C1</td>
									<td>2017-05-01 上午</td>
									<td>正常</td>
								</tr>
								<tr>
									<td>1</td>
									<td>张三</td>
									<td>18600001111</td>
									<td>C1</td>
									<td>2017-05-01 上午</td>
									<td>已取消</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../include/scripts.jsp"%>
<script>
	$('#queryStartTime').datetimepicker({format: 'yyyy-mm-dd',minView:'month'});
	$('#queryEndTime').datetimepicker({format: 'yyyy-mm-dd',minView:'month'});
</script>


</body>
</html>
