<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<base href="${BaseContext}>">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>课程设置</title>
<%@include file="../include/styles.jsp"%>
<%@include file="../include/scripts.jsp"%>

<style>
.passed {
	background-color: lightgrey;
}

.passed.schedued {
	background-color: yellow;
}

.unpassed.schedued {
	background-color: greenyellow;
}
</style>

<script type="text/javascript">
	function monthReilef(month) {
		$("#month").val(month);
		$("#formSubmit").submit();
	}
</script>

</head>
<body>


	<div id="wrapper">
		<jsp:include page="../include/header_coa.jsp"></jsp:include>
		<div id="page-wrapper">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="page-header">课程设置</h3>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<form action="coach/monthBeans.do" id="formSubmit">
				<input type="hidden" name="year" value="${dateEQ.year}" id="year" />
				<input type="hidden" name="month" id="month" value="${dateEQ.month}" /> 
				<input type="hidden" name="day" value="${dateEQ.day}" id="day" />
			</form>
			<nav aria-label="...">
				<ul class="pager">
					<li class="previous"><a onclick="monthReilef(${dateEQ.month-1})"><span
							aria-hidden="true">&larr;</span> 上月</a></li>
					<li><span>${dateEQ.year}年${dateEQ.month}月</span></li>
					<li class="next"><a onclick="monthReilef(${dateEQ.month+1})">下月<span
							aria-hidden="true">&rarr;</span>
					</a></li>
				</ul>
			</nav>

			<div class="row">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td>周日</td>
							<td>周一</td>
							<td>周二</td>
							<td>周三</td>
							<td>周四</td>
							<td>周五</td>
							<td>周六</td>
						</tr>
					</thead>
					<tbody>

						<c:forEach begin="1" end="${dateEQ.day}" varStatus="i" var="a">
							<!-- first 第一次 -->
							<c:if test="${i.first}">
								<tr>
							</c:if>
							<c:if test="${i.first}">
								<!-- 首先循环空格 -->
								<c:forEach begin="1" end="${dateEQ.week-1}">
									<td></td>
								</c:forEach>
							</c:if>
							<!-- 拿到是否有课程 arranged -->
							<c:set value="${dateEQ.monthBeans[a-1].arranged}" var="dateStatus" />
						
							<c:set value="${dateEQ.intraday}" var="intraday" />
							<td
								<c:if test="${i.count <= intraday}">
										<c:if test="${dateStatus==1}" >
											
											class="passed schedued" 	
										</c:if>
								<c:if test="${dateStatus == 0}" >
											
											class="passed" 		
										</c:if></c:if>

								<c:if test="${i.count > intraday}">
									<c:if test="${dateStatus == 1}">
											
											class="unpassed schedued"
									</c:if>
									<c:if test="${dateStatus == 0}">
											
											class="unpassed"  		
									</c:if>
								</c:if>
							>${a}</td>
							<!-- 进行判断 输出 最外面的的循环 count循环几次-->
							<!-- 循环的次数加上空格 得出总的td 对7 取余  换行 -->
							<c:if test="${i.last || (i.count+dateEQ.week-1)%7==0}">
								</tr>
							</c:if>
						</c:forEach>

					</tbody>
				</table>

				<table>
					<tr>
						<td style="background-color: lightgrey;">不可操作日期</td>
						<td style="background-color: yellow;">已过期的课程</td>
						<td style="background-color: greenyellow;">已安排的课程</td>
					</tr>
				</table>
			</div>

		</div>

	</div>
	<!-- 不可编辑的模态框 已过期时间 -->
	<div class="modal fade" id="passedModal" tabindex="1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="passedTitle">Modal title</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-success" id="pTitle">今天已安排课程！</div>
					<div class="alert alert-warning">过去的日期不能修改,勾选上的表示您该时段有排课。</div>
					<div class="checkbox">
						<label class="checkbox-inline"><input type="checkbox"
							id="noonP" value="上午">上午(3h)</label> <label
							class="checkbox-inline"><input type="checkbox"
							id="afternoonP" value="下午">下午(4h)</label> <label
							class="checkbox-inline"><input type="checkbox"
							id="nightP" value="晚上">晚上(2h)</label>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

				</div>
			</div>
		</div>
	</div>
	<%--不可编辑的模态框，已过期的时间--%>
	<%--可编辑的模态框，已过期的时间--%>
	<div class="modal fade" id="editModal" tabindex="1" role="dialog"
		aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="editedTitle">Modal title</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-success" id="titleE">今天已安排课程！</div>
					<div class="alert alert-warning">当不能选择时，表示您该课程已经被预约，不能进行操作。未预约的可以删除，未安排的时段可以安排</div>
					<form role="form" id="scheduleForm"  method="get">
						<input type="hidden" name="yearMonthDay" id="yearMonthDay" value="yearMonthDay"/>
						<input type="hidden" name="time" id="time" />
						<div class="form-group">
							<div class="checkbox">
								<label class="checkbox-inline">
								<input type="checkbox" name="timeStatus" id="noonE" value="上午">上午(3h)</label> 
								<label class="checkbox-inline">
								<input type="checkbox" name="timeStatus" id="afternoonE" value="下午">下午(4h)</label> 
								<label class="checkbox-inline">
								<input type="checkbox" name="timeStatus" id="nightE" value="晚上">晚上(2h)</label>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-default" onclick="save()">保存</button>
				</div>
			</div>

		</div>
	</div>
	<%--不可编辑的模态框，已过期的时间--%>
	<script>
   
    /*未过期时间处理*/
    $('td.unpassed').each(function () {
        $(this).on('click', function () {
            console.log('点击了：' + $(this).html());
            
            $("#day").val($(this).html());
           
            if ($(this).hasClass('schedued')) {
            	 alert("123");
                /*有安排时间，查询上中晚，同时查询是否被预约*/
                $.get("coach/selectSchedule.do", $("#formSubmit").serialize(), function (data) {
                   // var h = data.ymd;
                    $('#editedTitle').html(h + " 课程安排");
                    $("#yearMonthDay").val(h);
                  //  var datas = data.datas;
                    $("#titleE").html("今天已经安排课程！");
                    for(var i=0;i<data.length;i++){
                    	if(data[i].timestatus == "上午"){
                    		 $("#noonE").attr("checked", true);
                    	}
                    	if(data[i].timestatus == "下午"){
                    		 $("#afternoonE").attr("checked", true);
                    	}
                    	if(data[i].timestatus == "下午"){
                    		 $("#nightE").attr("checked", true);
                    	}
                    }
                    
                   /*  for (var key in datas) {
                        console.log(key + "-----" + datas[key].orderStatus);
                        if (datas[key].orderStatus == 1) {
                            if (key == h + "-上午") {
                                $("#noonE").attr("checked", true);
                            }
                            if (key == h + "-下午") {
                                $("#afternoonE").attr("checked", true);
                            }
                            if (key == h + "-晚上") {
                                $("#nightE").attr("checked", true);
                            }
                        } */
                        if (datas[key].orderStatus == 0) {
                            if (key == h + "-上午") {
                                $("#noonE").attr("checked", true);
                                $("#noonE").attr("disabled", true);
                                $("#noonE").attr("title", "该课程已经被预约，不可以取消");
                            }
                            if (key == h + "-下午") {
                                $("#afternoonE").attr("checked", true);
                                $("#afternoonE").attr("disabled", true);
                                $("#afternoonE").attr("title", "该课程已经被预约，不可以取消");
                            }
                            if (key == h + "-晚上") {
                                $("#nightE").attr("checked", true);
                                $("#nightE").attr("disabled", true);
                                $("#nightE").attr("title", "该课程已经被预约，不可以取消");
                            }
                        }
                    }
                , "json");
            } else {
            	
            	alert($(this).html())
            	var y = $(this).html();
            	var p=$("#yearMonthDay").val(y);
            
            	
                $.get("coach/selectSchedule.do",$("#scheduleForm").serialize(), function (data) {
                	alert(data);
                    if(data=="success"){
                    	
                    }
                    $('#editedTitle').html(h + " 课程安排");
                    $("#yearMonthDay").val(h);
                    $("#titleE").html("今天未安排课程！");
                }, "json");
            }
            //弹窗显示课程安排
            $('#editModal').modal('show');
        });
    });
    /*过期时间处理*/
    $('td.passed').each(function () {
        $(this).on('click', function () {
            console.log('点击了：' + $(this).html());
            $("#day").val($(this).html());
            $("#noonP").attr("disabled", true);
            $("#afternoonP").attr("disabled", true);
            $("#nightP").attr("disabled", true);
            $("input").attr("readOnly", true);
            if ($(this).hasClass('scheduled')) {
                $.get("coach/selectSchedule.do", $("#formSubmit").serialize(), function (data) {
                    var h = data.ymd;
                    $('#passedTitle').html(h + " 课程安排");
                    $("#yearMonthDay").val(h);
                    $("#pTitle").html("今天已经安排课程！");
                    console.log(data);
                    var datas = data.datas;
                    for (var key in datas) {
                        console.log(key + "-----" + datas[key].orderStatus);
                        if (key == h + "-上午") {
                            $("#noonP").attr("checked", true);
                        }
                        if (key == h + "-下午") {
                            $("#afternoonP").attr("checked", true);
                        }
                        if (key == h + "-晚上") {
                            $("#nightP").attr("checked", true);
                        }
                    }
                }, "json");
            } else {
                $.get("coach/selectSchedule.do", $("#formSubmit").serialize(), function (data) {
                    var h = data.ymd;
                    $('#passedTitle').html(h + " 课程安排");
                    $("#pTitle").html("今天未安排课程！");
                }, "json");
            }
            //弹窗显示课程安排
            $('#passedModal').modal('show');
        });
    });

    $('#passedModal').on('hidden.bs.modal', function (e) {
        $("#noonP").attr("checked", false);
        $("#afternoonP").attr("checked", false);
        $("#nightP").attr("checked", false);
        $("#noonP").removeAttr("disabled");
        $("#afternoonP").removeAttr("disabled");
        $("#nightP").removeAttr("disabled");
        // do something...
    });
    $('#editModal').on('hidden.bs.modal', function (e) {
        $("#noonE").attr("checked", false);
        $("#afternoonE").attr("checked", false);
        $("#nightE").attr("checked", false);
        $("#noonE").removeAttr("disabled");
        $("#afternoonE").removeAttr("disabled");
        $("#nightE").removeAttr("disabled");
        // do something...
    });

</script>
	<script>
    /*修改排班信息的函数*/
    function save() {
    	  var y = $("#year").val();
    	  var m = $("#month").val();
    	  var d = $("#day").val();
    	  alert(y+"-"+m+"-"+d);
    	  var timeStatus = "";
    	  $("input:checkbox[name='timeStatus']:checked").each(function(){
    		  timeStatus +=$(this).val()+"-";
    		  
		});
    	  
    	var p= {"timeStatus":timeStatus,"Y":y,"M":m,"D":d};
         $.post("coach/updateSchedule.do", p, function (data) {
            if (data=="success") {
                alert("保存成功");
            } else {
                alert("修改成功");
            }
            $('#editModal').modal('hide');
            location.reload();
            console.log(data)
        }); 
    }
</script>

</body>
</html>
