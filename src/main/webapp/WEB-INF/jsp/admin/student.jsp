<%--
  Created by IntelliJ IDEA.
  User: zhouyc
  Date: 2017/5/23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>后台学员管理首页</title>
	<%@include file="../include/styles.jsp"%>
</head>
<body>

<div id="wrapper">
	<jsp:include  page="../include/header.jsp"></jsp:include>

	<div id="page-wrapper">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="page-header">学员信息管理</h3>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading">
				<form class="form-inline">
					<div class="form-group">
						<label for="exampleInputName12">姓名</label>
						<input type="text" class="form-control" id="exampleInputName12" placeholder="">
					</div>
					<div class="form-group">
						<label for="exampleInputName22">手机号</label>
						<input type="text" class="form-control" id="exampleInputName22" placeholder="">
					</div>
					<div class="form-group">
						<label for="exampleInputEmail2">驾照级别</label>
						<select class="form-control" id="exampleInputEmail2">
							<option>C1</option>
							<option>C2</option>
							<option>C3</option>
							<option>C4</option>
							<option>A1</option>
							<option>A2</option>
							<option>A3</option>
							<option>B1</option>
							<option>B2</option>

						</select>
					</div>
					<button type="submit" class="btn btn-default">查询</button>
				</form>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
						<tr>
							<th>序号</th>
							<th>手机号</th>
							<th>姓名</th>
							<th>性别</th>
							<th>年龄</th>
							<th>驾照级别</th>
							<th>居住地址</th>
							<th>注册时间</th>
							<th>操作</th>
						</tr>
						</thead>
						<tbody>
						<tr>
							<td>1</td>
							<td>15888889999</td>
							<td>张三</td>
							<td>男</td>
							<td>26</td>
							<td>C1</td>
							<td>郑州市经开区</td>
							<td>2017-06-01</td>
							<td><button class="btn btn-primary" title="点击显示所有详细信息">查看</button></td>

						</tr>
						<tr>
							<td>2</td>
							<td>15888889999</td>
							<td>张三</td>
							<td>男</td>
							<td>26</td>
							<td>C1</td>
							<td>郑州市经开区</td>
							<td>2017-06-01</td>
							<td><button class="btn btn-primary">查看</button></td>

						</tr>
						<tr>
							<td>3</td>
							<td>15888889999</td>
							<td>张三</td>
							<td>男</td>
							<td>26</td>
							<td>C1</td>
							<td>郑州市经开区</td>
							<td>2017-06-01</td>
							<td><button class="btn btn-primary">查看</button></td>

						</tr>
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
		</div>
	</div>
</div>

<%@include file="../include/scripts.jsp"%>


</body>
</html>
