<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
	<base href="${BaseContext}>">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>登录页</title>
	<%@include file="include/styles.jsp"%>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">登录驾校易考平台</h3>
				</div>
				<div class="panel-body">
					<form role="form">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="telphone" name="mobile" type="email" autofocus id="mobile">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="password" name="password" type="password" value="" id="password">
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="student" id="optionsRadios1" value="student" >
									我是学员
								</label>
								&nbsp;
								<label>
								<input type="radio" name="student" id="optionsRadios2" value="coach">
								我是教练
							</label>

							</div>
							<p><a href="javascript:void(0);" class="btn btn-lg btn-success btn-block" onclick="goLogin();">登录</a></p>
							<p  class="text-right"><a href="home/regist.do">没有账号，去注册...</a></p>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<%@include file="include/scripts.jsp"%>
<script>

	function goLogin() {
		var v = $('input:radio:checked').val()
		var mobile = $("#mobile").val();
		var password = $("#password").val();
		if(v=='student'){
			
			window.location.href='student/index.do?mobile='+mobile+'&password='+password;
		}else{
			
			window.location.href='coach/go/index.do?mobile='+mobile+'&password='+password;
		}
	}
</script>

</body>
</html>
