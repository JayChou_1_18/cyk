<%--
  User: zhouyanchao
  Date: 2017/5/23
--%>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<base href="${BaseContext}>">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>注册页</title>
<%@include file="include/styles.jsp"%>

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading">注册成为平台会员</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<div class="alert alert-info">
							请根据自己的需要选择会员身份，身份信息一旦注册不可更改，请谨慎选择！如果您已有账号，请去<a href="gologin.do"
								class="alert-link">登录页面</a>.
						</div>
						<!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a href="#home" data-toggle="tab">我是学员</a>
							</li>
							<li><a href="#profile" data-toggle="tab">我是教练</a></li>

						</ul>

						<div class="tab-content">
							<div class="tab-pane fade in active" id="home">
								<form role="form" action="home/login.do">
									<div class="form-group">
										<%--<label>手机号</label>--%>
										<input type="text" class="form-control" placeholder="输入手机号码"
											name="mobile" id="studentMobile">
										<p class="help-block">手机号是登录的唯一凭证，请谨慎填写</p>
										<span id="spanMobile"></span>
									</div>
									<div class="form-group">
										<%--<label>登录密码</label>--%>
										<input type="password" class="form-control"
											placeholder="输入登录密码" name="password" id="studentPass">
									</div>
									<div class="form-group">
										<%--<label>确认登录密码</label>--%>
										<input type="password" class="form-control"
											placeholder="确认登录密码" name="password2" id="studentPass2">
										<span id="span"></span>
									</div>
									<div class="form-group">
										<%--<label>驾照级别</label>--%>
										<select class="form-control" name="licenselevel">
											<option>C1</option>
											<option>C2</option>
											<option>C3</option>
											<option>C4</option>
											<option>A1</option>
											<option>A2</option>
											<option>A3</option>
											<option>B1</option>
											<option>B2</option>

										</select>
										<p class="help-block">选择您要考的驾照级别，提交后不可更改，请谨慎填写</p>

									</div>
									<button type="submit" onclick="return studentRegist()"
										class="btn btn-default">注册为学员</button>
								</form>
							</div>

							<!--教练  -->
							<div class="tab-pane fade" id="profile">
								<form role="form" action="home/coachLogin.do">
									<div class="form-group">
										<%--<label>手机号</label>--%>
										<input type="text" class="form-control" placeholder="输入手机号码"
											name="mobile" id="coachMobile">
										<p class="help-block">手机号是登录的唯一凭证，请谨慎填写</p>
										<span id="mobileSpan"></span>
									</div>
									<div class="form-group">
										<%--<label>登录密码</label>--%>
										<input type="password" class="form-control"
											placeholder="输入登录密码" name="password2" id="coachPassword">
									</div>
									<div class="form-group">
										<%--<label>确认登录密码</label>--%>
										<input type="password" class="form-control"
											placeholder="确认登录密码" name="password" id="coachPassword2">
										<span id="passSpan"></span>
									</div>
									<div class="form-group">
										<%--<label>证书级别</label>--%>
										<select class="form-control" name="certificate">
											<option>C1</option>
											<option>C2</option>
											<option>C3</option>
											<option>C4</option>
											<option>A1</option>
											<option>A2</option>
											<option>A3</option>
											<option>B1</option>
											<option>B2</option>

										</select>
										<p class="help-block">选择您的资格证书级别，提交后不可更改，请谨慎填写</p>

									</div>
									<button type="submit" onclick="return coachRegist()"
										class="btn btn-default">注册为教练</button>
								</form>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	</div>

	<%@include file="include/scripts.jsp"%>
	<script type="text/javascript">
		var regIsMobile = false;
		var regIsPassword = false;
		//学生注册
		$(function(){
			$("#studentMobile").blur(function(){
					var mobile = $("#studentMobile").val();
					if(null != mobile && mobile != ""){
					var params = {"mobile" : mobile};
					
						$.post("home/ajaxStudent.do", params, function(data) {
							
							if (data == "success") {
								regIsMobile = true;
								$("#spanMobile").text("该手机号可用").css("color", "red");

							} else {
								regIsMobile = false;
								$("#spanMobile").text("该手机已注册，请从新输入").css("color", "red");
							}

						});
					}else{
						$("#spanMobile").text("手机号不能为空").css("color", "red");
						regIsMobile = false;
					}
				});
			
			
			
			$("#studentPass2").blur(function(){
				var pass = $("#studentPass").val();
				var pass2 = $("#studentPass2").val();
				if (pass == "" || null == pass || null == pass2 || pass2 == "") {
					$("#span").text("密码不能为空").css("color", "red");
					regIsPassword = false;
				} else {
					if (pass != pass2) {
						$("#span").text("密码错误").css("color", "red");
						regIsPassword = false;
					} else {
						$("#span").text("");
						regIsPassword = true;
					}
				}
			});
			
		});
		
		function studentRegist() {
			if(regIsMobile && regIsPassword){
				return true;
			}else{
				return false;
			}
		}
	
	</script>
	
	<script type="text/javascript">
		//教练注册
		var regIsMobile = false;
		var regIsPassword = false;
		$(function() {
			$("#coachMobile").blur(function(){
				var coachMobile = $("#coachMobile").val();
				if(null !=coachMobile && coachMobile != "" ){
					var params = {"mobile" : coachMobile};
					$.post("ajaxCoach.do", params, function(data) {
						if (data == "success") {
							regIsMobile = true;
							$("#mobileSpan").text("该手机号可用").css("color", "red");
						} else {
							regIsMobile = false;
							$("#mobileSpan").text("该手机已注册，请从新输入").css("color", "red");
						}

					});
				}else{
					$("#mobileSpan").text("手机号不能为空").css("color", "red");
					regIsMobile = false
				}
			});
			
			$("#coachPassword2").blur(function(){
				var pass = $("#coachPassword").val();
				var pass2 = $("#coachPassword2").val();
				if (pass == "" || null == pass || null == pass2 || pass2 == "") {
					$("#passSpan").text("密码不能为空").css("color", "red");
					regIsPassword = false;
				} else {
					if (pass != pass2) {
						$("#passSpan").text("密码错误").css("color", "red");
						regIsPassword = false;
					} else {
						$("#passSpan").text("");
						regIsPassword = true;
					}
				}
			})
			
		});
		
		function coachRegist() {
			if(regIsMobile && regIsPassword){
				return true;
			}else{
				return false;
			}
		}
	
	</script>

	<!-- <script type="text/javascript">
			var pas = $("#coachPassword").val();
			var pas2 = $("#coachPassword2").val();
			var coachMobile = $("#coachMobile").val();
			if (regIsCommitPsw) {
				$("#coachMobile").blur(
						function() {

							if (null != coachMobile && coachMobile != "") {
								var params = {
									"mobile" : coachMobile
								};
								$.post("ajaxCoach.do", params, function(data) {
									if (data == "success") {
										$("#mobileSpan").text("该手机可用").css(
												"color", "red");
										regIsCommitPsw = true;
									} else {
										$("#mobileSpan").text("该手机已注册，请从新输入")
												.css("color", "red");
										regIsCommitPsw = false;
									}
								})
							}
							regIsCommitPsw = false;
						});
			}

		});

		function coachRegist() {

			var pass = $("#coachPassword").val();
			var pass2 = $("#coachPassword2").val();
			if (pass == "" || null == pass || pass2 == "" || null == pass2) {
				$("#passSpan").text("密码不能为空").css("color", "red");
				
			} else {
				if (pass != pass2) {
					$("#passSpan").text("密码错误").css("color", "red");
					
				} else {
					$("#passSPan").text("");
					if(regIsCommitPsw){
						return true;
					}
				return false;
					
				}
			}
			return false;
		}

		function studentRegist() {

			var pass = $("#studentPass").val();
			var pass2 = $("#studentPass2").val();
			if (pass == "" || null == pass || null == pass2 || pass2 == "") {
				$("#span").text("密码不能为空").css("color", "red");
				regIsCommitPsw = false;
			} else {
				if (pass != pass2) {
					$("#span").text("密码错误").css("color", "red");
					regIsCommitPsw = false;
				} else {
					$("#span").text("");
					regIsCommitPsw = true;
				}
			}
		}
	</script>
 -->
</body>
</html>
