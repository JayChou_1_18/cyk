package com.zhiyou100.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zhiyou100.pojo.Appointment;
import com.zhiyou100.pojo.AppointmentEQ;
import com.zhiyou100.pojo.Coach;
import com.zhiyou100.pojo.DateEQ;
import com.zhiyou100.pojo.ExamAnswer;
import com.zhiyou100.pojo.ExamResult;
import com.zhiyou100.pojo.ExamTopic;
import com.zhiyou100.pojo.MonthBeans;
import com.zhiyou100.pojo.Schedule;
import com.zhiyou100.pojo.Student;
import com.zhiyou100.pojo.Topic;
import com.zhiyou100.service.AppointmentService;
import com.zhiyou100.service.CoachService;
import com.zhiyou100.service.ExamAnswerService;
import com.zhiyou100.service.ExamResultService;
import com.zhiyou100.service.ExamTopicService;
import com.zhiyou100.service.ScheduleService;
import com.zhiyou100.service.StudentService;

@Controller
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@Autowired
	private CoachService coachService;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Autowired
	private ExamTopicService examTopocService;
	
	@Autowired
	private ExamResultService examResultService;
	
	@Autowired
	private ExamAnswerService examAnswerService;
	
	
	//退出
	@RequestMapping("/remove/login.do")
	public String homeLogin(HttpSession session){
		session.removeAttribute("studentMobile");
		return "/WEB-INF/jsp/login.jsp";
	}
	
	//进入到学员首页
	@RequestMapping("/student/index.do")
	public String studentIndex(Student student , HttpSession session){
		String studentMobile = (String) session.getAttribute("studentMobile");
		if(null != studentMobile){
			return "/WEB-INF/jsp/student/index.jsp";
		}
		int index = studentService.selectStudentLoginIndex(student);
		if(index>0){
			session.setAttribute("studentMobile", student.getMobile());
			return "/WEB-INF/jsp/student/index.jsp";
		}
		return "/WEB-INF/jsp/login.jsp";
	}
	
	//学员个人信息
	@RequestMapping("/student/profile.do")
	public String profile(HttpSession session , Model model){
		String mobile = (String) session.getAttribute("studentMobile");
		Student student =  studentService.selectStudentByMobile(mobile);
		model.addAttribute("student",student);
		return "/WEB-INF/jsp/student/profile.jsp";
	}
	
	//更新学员信息
	@RequestMapping("/student/update.do")
	public String studentUpdate( Model model , Student student){
		studentService.updeatStudent(student);
		
		return "redirect:/student/profile.do";
	}
	
	//查看预约课程
	@RequestMapping("/student/classes.do")
	public String studentClasses(Model model){
		List<AppointmentEQ> appointmentList = appointmentService.selectAppointmentService();
		model.addAttribute("appointmentList",appointmentList);
		return "/WEB-INF/jsp/student/classes.jsp";
	}
	
	//进入抢教练页面
	@RequestMapping("/student/coach.do")
	public String studentCoach(Model model){
		//查找教练所有教练
		List<Coach> coachs = coachService.selectAddCoach();
		model.addAttribute("coachList",coachs);
		
		return "/WEB-INF/jsp/student/coach.jsp";
	}
	
	//展示教练详细信息
	@RequestMapping("/student/showCoachMessage.do")
	public String showCoachMessage(int id,Model model) throws ParseException{
			
			Coach coach = coachService.selectCoachById(id);
			
			//查看支教课程
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			int intraday = calendar.get(Calendar.DAY_OF_MONTH);
			int month = calendar.get(Calendar.MONTH)+1;
			calendar.set(Calendar.DAY_OF_MONTH,1);
			int week = calendar.get(Calendar.DAY_OF_WEEK);
			int day = calendar.getActualMaximum(Calendar.DATE);
			
			List<Schedule> scheduleList = scheduleService.selectScheduleByCoachId(id);
			
			
			
			List<MonthBeans>  monthBeansList = new ArrayList<MonthBeans>(); 
			for(int i=0;i<day;i++){
				MonthBeans monthBeans = new MonthBeans();
				for(int j=0;j<scheduleList.size();j++){
					Date date = scheduleList.get(j).getArrangedate();
					SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
					String zDate = format.format(date);
					String[] split = zDate.split("-");
					int arrangeDate = Integer.valueOf(split[2]); 
					if(i+1 == arrangeDate){
						//通过教练ID和时间查出今天的课程
						//首先拼出今天的时间
						String J = year+"-"+month+"-"+arrangeDate;
						//将时间转成Date
						String a="";
						Date parse = format.parse(J);
						List<Schedule> schedules = scheduleService.selectScheduleByCochIdAndData(id,parse);
						for (Schedule schedule2 : schedules) {
							//String scheduless = schedule2.getTimestatus();
							a+=schedule2.getTimestatus();
						}
						monthBeans.setTimestatus(a);
						a="";
						monthBeans.setArranged(1);//代表有课程
						break;
					}else{
						monthBeans.setArranged(0);//代表没有课程
					}
				}
				monthBeansList.add(monthBeans);
			}
			
			DateEQ dateEQ = new DateEQ();
			dateEQ.setDay(day);
			dateEQ.setIntraday(intraday);
			dateEQ.setYear(year);
			dateEQ.setMonth(month);
			dateEQ.setWeek(week);
			dateEQ.setMonthBeans(monthBeansList);
			model.addAttribute(dateEQ);
			model.addAttribute(coach);
			return "/WEB-INF/jsp/student/coachInfo.jsp";
	}
	
	//进入报名页面
	@RequestMapping("student/apply.do")
	public String studentApply(int id , Model model){
		Coach coach = coachService.selectCoachById(id);
		
		//选择日历
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int intraday = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH)+1;
		calendar.set(Calendar.DAY_OF_MONTH,1);
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		int day = calendar.getActualMaximum(Calendar.DATE);
		//通过教练ID 查询出所有的课程  
		List<Schedule> scheduleList = scheduleService.selectScheduleByCoachId(coach.getId());
		List<MonthBeans>  monthBeansList = new ArrayList<MonthBeans>(); 
		for(int i=0;i<day;i++){
			MonthBeans monthBeans = new MonthBeans();
			for(int j=0;j<scheduleList.size();j++){
				Date date = scheduleList.get(j).getArrangedate();
				SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd");
				String zDate = format.format(date);
				String[] split = zDate.split("-");
				int arrangeDate = Integer.valueOf(split[2]); 
				if(i+1 == arrangeDate){
					monthBeans.setArranged(1);//代表有课程
					break;
				}else{
					monthBeans.setArranged(0);//代表没有课程
				}
			}
			monthBeansList.add(monthBeans);
		}
		DateEQ dateEQ = new DateEQ();
		dateEQ.setDay(day);
		dateEQ.setIntraday(intraday);
		dateEQ.setYear(year);
		dateEQ.setMonth(month);
		dateEQ.setWeek(week);
		dateEQ.setMonthBeans(monthBeansList);
		model.addAttribute(dateEQ);
		
		
		String coachName = coach.getName();
		int hoursFee = coach.getHoursfee();
		String certificate = coach.getCertificate();
		Coach addCoach = new Coach();
		addCoach.setId(coach.getId());
		addCoach.setName(coachName);
		addCoach.setHoursfee(hoursFee);
		addCoach.setCertificate(certificate);
		model.addAttribute("addCoach",addCoach);
		return "/WEB-INF/jsp/student/apply.jsp";
	}
	
	
	//提交报名
	@RequestMapping("student/applySuccess.do")
	public String studentApplySuccess(AppointmentEQ appointmentEQ,Model model,HttpSession session){
		String mobile = (String) session.getAttribute("studentMobile");
		Student student = studentService.selectStudentByMobile(mobile);
		int studentId = student.getId();
		String studentName = appointmentEQ.getStudentName();
		String studentMobile = appointmentEQ.getStudentMobile();
		int coachId = appointmentEQ.getCoachId();
		
		Coach coach = coachService.selectCoachById(coachId);
		String coachName = coach.getName();
		
	    String coachMobile = coach.getMobile();
	    String licenseLevel = appointmentEQ.getLicenseLevel();
	    String applyMoney = appointmentEQ.getApplyMoney();
	    String timeSchedule = appointmentEQ.getTimeSchedule();
	    String addSchedule = appointmentEQ.getAddSchedule();
	    String [] adds = addSchedule.split("/");
	    String schedule = "";
	    for(int i=0;i<adds.length;i++){
	    	schedule = adds[i];
	    	AppointmentEQ eq = new AppointmentEQ();
			eq.setStudentId(studentId);
			eq.setStudentName(studentName);
			eq.setStudentMobile(studentMobile);
			eq.setCoachId(coachId);
			eq.setCoachName(coachName);
			eq.setCoachMobile(coachMobile);
			eq.setLicenseLevel(licenseLevel);
			eq.setTimeSchedule(timeSchedule);
			eq.setApplyMoney(applyMoney);
			eq.setAddSchedule(schedule);
			appointmentService.insertAppointmentService(eq);
	    }
		List<AppointmentEQ> appointmentList = appointmentService.selectAppointmentService(studentId);
		model.addAttribute("appointmentList",appointmentList);
		return "/WEB-INF/jsp/student/classes.jsp";
	}
	
	//修改课程状态
	@RequestMapping("student/updateMessageState.do")
	@ResponseBody
	public String studentUpdateState(String id){
		int jay = appointmentService.updateAppointmentState(Integer.parseInt(id));
		if(jay>0){
			
			return "success";
		}else{
			return "error";
		}
		
	}
	
	//在线考试
	@RequestMapping("student/examOldResule.do")
	
	public String examOldResule(){
		
		List<ExamTopic> topic = examTopocService.selectAllExamTopic();
		List<ExamResult> examResults = examResultService.selectAllExamResult();
		List<ExamAnswer> currentAnswer = examAnswerService.selectAllExamAnswer();
		
		
		return "/WEB-INF/jsp/student/examOnline.jsp";
	}
	
	
	@RequestMapping("student/topic.do")
	@ResponseBody
	public ModelAndView studentTopic(Topic topic){
		Topic topic2 = examTopocService.selectExamTopicById(topic.getTid());
	    List<ExamAnswer> answersList = examAnswerService.selectExamAnswerByTid(topic.getTid());	
	    topic2.setAnswerList(answersList);
		ModelAndView mView = new ModelAndView();
		mView.addObject("topic", topic2);
		mView.setViewName("/WEB-INF/jsp/student/topic.jsp");
		return mView;
	}
	
	@RequestMapping("student/answer.do")
	@ResponseBody
	public String studentAnswer(int topicid,String answer,HttpSession session,Model model){
		String mobile = (String) session.getAttribute("studentMobile");
		Student student = studentService.selectStudentByMobile(mobile);
		
		ExamAnswer examAnswer = examResultService.selectExamResultByTopicIdAndSort(topicid,1);
		
		examResultService.insertExamEesult(topicid,student.getId(),answer,examAnswer.getAsvalue());
		
		
		
		return "/WEB-INF/jsp/student/topic.jsp";
	}
	
	
}
