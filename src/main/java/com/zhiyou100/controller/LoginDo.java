package com.zhiyou100.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zhiyou100.pojo.Student;
import com.zhiyou100.service.CoachService;
import com.zhiyou100.service.StudentService;

@Controller
public class LoginDo {
	
	@Autowired
	private StudentService studentService; 
	
	@Autowired
	private CoachService coachService;
	
	//到首页
	@RequestMapping("/login.do")
	public String login(){
		
		
		return "/WEB-INF/jsp/login.jsp";
	}
	
	@RequestMapping("/gologin.do")
	public String goLogin(){
		return "redirect:/login.do";
	}
	
	//注册页面
	@RequestMapping("home/regist.do")
	public String regist(){
		
		return "/WEB-INF/jsp/regist.jsp";
	}
	
	//提交数据
	@RequestMapping("home/login.do")
	public String login(Student student){
		System.out.println(student.getMobile()+","+student.getPassword()+"==========================");
		int re = studentService.selectStudentBymobile(student);
		System.out.println("注册返回数"+re);
		if(re>0){
			return  "/WEB-INF/jsp/login.jsp";
		}
		
		return  "redirect:/home/login.do";
	}
	
	@RequestMapping("home/ajaxStudent.do")
	@ResponseBody
	public String ajaxStudent(String mobile){
		System.out.println("学员注册手机ajax效验"+mobile);
		return  studentService.selectStudentBymobile(mobile);
	}
	
	//ajax效验教练帐号
	@RequestMapping("ajaxCoach.do")
	@ResponseBody
	public String ajaxCoach(String mobile){
		return coachService.countCoachByMobile(mobile);
	}
	
	
}
