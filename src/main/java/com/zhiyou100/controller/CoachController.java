package com.zhiyou100.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zhiyou100.pojo.Coach;
import com.zhiyou100.pojo.DateEQ;
import com.zhiyou100.pojo.MonthBeans;
import com.zhiyou100.pojo.Schedule;
import com.zhiyou100.pojo.UpdateSchedule;
import com.zhiyou100.service.CoachService;
import com.zhiyou100.service.ScheduleService;

@Controller
public class CoachController {
	
	@Autowired
	private CoachService coachService;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Value("${HEAD_IMG}")
	private String HEAD_IMG;
	
	@Value("${IMG_SEARVER_URL}")
	private String IMG_SEARVER_URL;
	
	//注册教练
	@RequestMapping("/home/coachLogin.do")
	public String homeCoachLogin(Coach coach){
		coachService.insertCoach(coach);
		return "/WEB-INF/jsp/login.jsp";
	}
	
	//进入教练主页
	@RequestMapping("/coach/go/index.do")
	public String coachGoIndex(HttpSession session,Coach coach){
		
		int reg = coachService.countCoachIndex(coach);
		if(reg>0){
			session.setAttribute("coachMobile", coach.getMobile());
			return "/WEB-INF/jsp/coach/index.jsp";
		}
		
		
		return "/WEB-INF/jsp/login.jsp";
	}
	//主页
	@RequestMapping("/coach/index.do")
	public String coachIndex(HttpSession session,Model model){
		String mobile = (String) session.getAttribute("coachMobile");
		Coach coach = coachService.selectCoachByMobile(mobile);
		model.addAttribute(coach);
		return "/WEB-INF/jsp/coach/index.jsp";
	}
	
	//进入教练个人信息
	@RequestMapping("/coach/profile.do")
	public String coachProfile(HttpSession session , Model model){
		String mobile = (String) session.getAttribute("coachMobile");
		Coach coach = coachService.selectCoach(mobile);
		model.addAttribute("coach",coach);
		return "/WEB-INF/jsp/coach/profile.jsp";
	}
	
	//更新教练资料
	@RequestMapping("/coach/updateAll.do")
	public String coachUpdateAll(Coach coach,MultipartFile imageFile,HttpServletRequest request) throws IllegalStateException, IOException{
		//更新教练头像
		String image = imageFile.getOriginalFilename();
		String seffor = image.substring(image.lastIndexOf("."));
		String newSeffor = UUID.randomUUID().toString();
		String newName = newSeffor + seffor;
		//提交图片 保存
		String path = request.getSession().getServletContext().getRealPath("img"); 
		imageFile.transferTo(new File(path,newName));
		 
		Date date = new Date();
 		coach.setUpdatetime(date);
 		//修改数据库中的路径
 		coach.setAvatar("img/"+newName);
		coachService.updeatCoachAll(coach);
		return "redirect:/coach/profile.do";
	}
	
	
	//教练课程设置
	@RequestMapping("coach/setting.do")
	public String coachSetting(Model model,HttpSession session){
		//首先获取当前 年、月、日 
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int intraday = calendar.get(Calendar.DAY_OF_MONTH);//今天的日期 
		int month = calendar.get(Calendar.MONTH)+1;
		calendar.set( Calendar.DAY_OF_MONTH, 1);//当前月的第一天是周几
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		int day = calendar.getActualMaximum(Calendar.DATE);//该月一共有多少天
		
		//通过教练ID查出 所对应的数据
		String mobile = (String) session.getAttribute("coachMobile");
		Coach coach = coachService.selectCoach(mobile);
		int coachId = coach.getId();
		List<Schedule> schedulesList = scheduleService.selectScheduleByCoachId(coachId);
		
		
		
		DateEQ dateEQ = new DateEQ();
		
		List<MonthBeans> list = new ArrayList<MonthBeans>();
		
		for (int i = 0; i < day; i++) {
			MonthBeans monthBeans = new MonthBeans();
			//2017-7-1
			for(int j=0;j<schedulesList.size();j++){
				Date date = schedulesList.get(j).getArrangedate();
				
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String format2 = format.format(date);
				String[] split = format2.split("-");
				int arrangeDate = Integer.valueOf(split[2]);
				if((i+1) == arrangeDate ){
					
					
					monthBeans.setArranged(1);//代表有课程
					break;
				}else{
					monthBeans.setArranged(0);//代表没有课程
					
				}
			}
			list.add(monthBeans);
		}
		
		dateEQ.setMonthBeans(list);
		dateEQ.setIntraday(intraday);
		dateEQ.setYear(year);
		dateEQ.setMonth(month);
		dateEQ.setDay(day);
		dateEQ.setWeek(week);
		model.addAttribute(dateEQ);
		return "/WEB-INF/jsp/coach/setting.jsp";
	}
	
	//教练  我的预约
	@RequestMapping("coach/classes.do")
	public String coachClasses(){
		
		return "/WEB-INF/jsp/coach/classes.jsp";
	}
	
	//上下月
	@RequestMapping("coach/monthBeans.do")
	public String monthMinus(DateEQ dateEQ , Model model){
		int year = dateEQ.getYear();
		int month = dateEQ.getMonth();
		int day = 0;
		if(month > 12){
			 year+=1;
			 month = 1;
		}
		if(month<1){
			year -=1;
			month =12;
		}
		if(month==1||month==3||month==5||month==7||month==8||month==10||month==12){
			day=31;
		}else if(month==2){
			if(year % 4== 0 && year % 100 !=0 || year% 400 == 0){
				day=29;
			}else{
				day=28;
			}
		}else{
			day=30;
		}
		//拿到月 判断这个月的第一天是周几
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR,year);
		calendar.set(Calendar.MONTH,month-1);
		calendar.set( Calendar.DAY_OF_MONTH, 1);
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		System.out.println("变化后的月份第一天是周几"+week);
		
		dateEQ.setYear(year);
		dateEQ.setMonth(month);
		dateEQ.setWeek(week);
		dateEQ.setDay(day);
		model.addAttribute(dateEQ);
		
		return "/WEB-INF/jsp/coach/setting.jsp";
	}
	
	//ajax判断查看是否有安排课程   未过期
	@RequestMapping("coach/updateSchedule.do")
	@ResponseBody
	public String coachSelectSchedule(UpdateSchedule updateSchedule, HttpSession session) throws ParseException{
		String mobile = (String) session.getAttribute("coachMobile");
		
		String Y = updateSchedule.getY();
		String M = updateSchedule.getM();
		String D = updateSchedule.getD();
		String arrang = Y+"-"+M+"-"+D;
		//将string类型转为date类型
		DateFormat format = new SimpleDateFormat("yyy-MM-dd");
		Date date = format.parse(arrang);
		//拿到 字符串 进行分割
		String time = updateSchedule.getTimeStatus();
		String pdd = time.trim();
		String[] split = pdd.split("-");
		Coach coach = coachService.selectCoach(mobile);
		for(int i=0;i<split.length;i++){
			UpdateSchedule up = new UpdateSchedule();
			up.setTimeStatus(split[i]);
			up.setCoachId(coach.getId());
			up.setArrangeDate(date);
			scheduleService.insertSchedule(up);
		}
		
		//点击时通过 教练ID 和  时间 查询 数据
		
		
		
		//看是否 有数据 有数据就是修改 没有就是添加
		/*List<Schedule> list = scheduleService.selectScheduleByCoachIdAndArrangeDate(up);
		if(list.size()>0){
			scheduleService.updateScheduleNoonE(up);
			scheduleService.updateScheduleAfternoonE(up);
			scheduleService.updateScheduleNightE(up);
			return "update";
		}
		*/
		 
		return "success";
	}
	//查询 显示勾选状态
	@RequestMapping("coach/selectSchedule.do")
	@ResponseBody
	public List<Schedule> coachSeleceSchedule(UpdateSchedule updateSchedule, HttpSession session) throws ParseException{
		String mobile = (String) session.getAttribute("coachMobile");
		
		String Y = updateSchedule.getY();
		String M = updateSchedule.getM();
		String D = updateSchedule.getD();
		String arrang = Y+"-"+M+"-"+D;
		//将string类型转为date类型
		DateFormat format = new SimpleDateFormat("yyy-MM-dd");
		Date date = format.parse(arrang);
		Coach coach = coachService.selectCoach(mobile);
		int coachId=coach.getId();
		List<Schedule> scheduleList = scheduleService.slelctScheduleByCoachIdAndArrangeDate(coachId,date);
		
		return scheduleList;
	}
	
	
}
