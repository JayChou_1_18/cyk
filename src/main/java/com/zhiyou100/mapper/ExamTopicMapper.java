package com.zhiyou100.mapper;

import com.zhiyou100.pojo.ExamTopic;
import com.zhiyou100.pojo.ExamTopicExample;
import com.zhiyou100.pojo.Topic;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExamTopicMapper {
    int countByExample(ExamTopicExample example);

    int deleteByExample(ExamTopicExample example);

    int deleteByPrimaryKey(Integer tid);

    int insert(ExamTopic record);

    int insertSelective(ExamTopic record);

    List<ExamTopic> selectByExample(ExamTopicExample example);

    ExamTopic selectByPrimaryKey(Integer tid);

    int updateByExampleSelective(@Param("record") ExamTopic record, @Param("example") ExamTopicExample example);

    int updateByExample(@Param("record") ExamTopic record, @Param("example") ExamTopicExample example);

    int updateByPrimaryKeySelective(ExamTopic record);

    int updateByPrimaryKey(ExamTopic record);

	List<ExamTopic> selectAllExamTopic();

	Topic selectExamTopicById(Integer tid);
}