package com.zhiyou100.mapper;

import com.zhiyou100.pojo.ExamAnswer;
import com.zhiyou100.pojo.ExamAnswerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExamAnswerMapper {
    int countByExample(ExamAnswerExample example);

    int deleteByExample(ExamAnswerExample example);

    int deleteByPrimaryKey(Integer aid);

    int insert(ExamAnswer record);

    int insertSelective(ExamAnswer record);

    List<ExamAnswer> selectByExample(ExamAnswerExample example);

    ExamAnswer selectByPrimaryKey(Integer aid);

    int updateByExampleSelective(@Param("record") ExamAnswer record, @Param("example") ExamAnswerExample example);

    int updateByExample(@Param("record") ExamAnswer record, @Param("example") ExamAnswerExample example);

    int updateByPrimaryKeySelective(ExamAnswer record);

    int updateByPrimaryKey(ExamAnswer record);

	List<ExamAnswer> selectAllExamAnswer();

	List<ExamAnswer> selectExamAnswerByTid(Integer integer);

	List<ExamAnswer> selectExamAnswerByTopicId(int topicid);

	

	
}