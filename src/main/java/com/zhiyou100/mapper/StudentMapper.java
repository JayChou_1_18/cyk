package com.zhiyou100.mapper;


import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.zhiyou100.pojo.Student;
import com.zhiyou100.pojo.StudentExample;

public interface StudentMapper {
    int countByExample1(StudentExample example);

    int deleteByExample(StudentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Student record);

    int insertSelective(Student record);

    List<Student> selectByExample(StudentExample example);

    Student selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Student record, @Param("example") StudentExample example);

    int updateByExample(@Param("record") Student record, @Param("example") StudentExample example);

    int updateByPrimaryKeySelective(Student record);

    int updateByPrimaryKey(Student record);

	int countStudentBymobile(String mobile);

	void insertStudent(Student student);

	Student selectStudentByMobile(String mobile);

	

	
}