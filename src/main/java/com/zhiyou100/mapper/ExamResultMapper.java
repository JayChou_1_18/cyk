package com.zhiyou100.mapper;

import com.zhiyou100.pojo.ExamAnswer;
import com.zhiyou100.pojo.ExamResult;
import com.zhiyou100.pojo.ExamResultExample;
import com.zhiyou100.pojo.Topic;

import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ExamResultMapper {
    int countByExample(ExamResultExample example);

    int deleteByExample(ExamResultExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ExamResult record);

    int insertSelective(ExamResult record);

    List<ExamResult> selectByExample(ExamResultExample example);

    ExamResult selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ExamResult record, @Param("example") ExamResultExample example);

    int updateByExample(@Param("record") ExamResult record, @Param("example") ExamResultExample example);

    int updateByPrimaryKeySelective(ExamResult record);

    int updateByPrimaryKey(ExamResult record);

	List<ExamResult> selectAllExamResult();


	Topic selectExamResultByTopicId(Integer topicid);

	ExamAnswer selectExamResultByTopicIdAndSort(ExamAnswer examAnswer);

	void insertExamResu(ExamResult examResult);
}