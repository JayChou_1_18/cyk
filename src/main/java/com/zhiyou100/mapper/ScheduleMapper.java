package com.zhiyou100.mapper;

import com.zhiyou100.pojo.Schedule;
import com.zhiyou100.pojo.ScheduleExample;
import com.zhiyou100.pojo.UpdateSchedule;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ScheduleMapper {
    int countByExample(ScheduleExample example);

    int deleteByExample(ScheduleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Schedule record);

    int insertSelective(Schedule record);

    List<Schedule> selectByExample(ScheduleExample example);

    Schedule selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Schedule record, @Param("example") ScheduleExample example);

    int updateByExample(@Param("record") Schedule record, @Param("example") ScheduleExample example);

    int updateByPrimaryKeySelective(Schedule record);

    int updateByPrimaryKey(Schedule record);

	List<Schedule> selectScheduleByCoachId(int id);

	int insertSchedule(UpdateSchedule up);

	

	

	void updateScheduleNoonE(UpdateSchedule up);

	void updateScheduleAfternoonE(UpdateSchedule up);

	void updateScheduleNightE(UpdateSchedule up);

	List<Schedule> selectScheduleByCoachIdAndArrangeDate(int coachId, Date date);

	List<Schedule> selectScheduleByCoachIdAndData(Schedule schedule);
}