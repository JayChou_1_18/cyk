package com.zhiyou100.mapper;

import com.zhiyou100.pojo.Coach;
import com.zhiyou100.pojo.CoachExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoachMapper {
    int countByExample(CoachExample example);

    int deleteByExample(CoachExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Coach record);

    int insertSelective(Coach record);

    List<Coach> selectByExample(CoachExample example);

    Coach selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Coach record, @Param("example") CoachExample example);

    int updateByExample(@Param("record") Coach record, @Param("example") CoachExample example);

    int updateByPrimaryKeySelective(Coach record);

    int updateByPrimaryKey(Coach record);

	

	int countCoachByMobile(String mobile);

	Coach selectCoach(String mobile);

	int countCoachIndex(Coach coach);

	Coach selectCoachByMobile(String mobile);

	List<Coach> selectAddCoach();

	Coach selectCoachById(Integer id);

	
}