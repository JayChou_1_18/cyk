package com.zhiyou100.mapper;

import com.zhiyou100.pojo.Appointment;
import com.zhiyou100.pojo.AppointmentEQ;
import com.zhiyou100.pojo.AppointmentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AppointmentMapper {
    int countByExample(AppointmentExample example);

    int deleteByExample(AppointmentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Appointment record);

    int insertSelective(Appointment record);

    List<Appointment> selectByExampleWithBLOBs(AppointmentExample example);

    List<Appointment> selectByExample(AppointmentExample example);

    Appointment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Appointment record, @Param("example") AppointmentExample example);

    int updateByExampleWithBLOBs(@Param("record") Appointment record, @Param("example") AppointmentExample example);

    int updateByExample(@Param("record") Appointment record, @Param("example") AppointmentExample example);

    int updateByPrimaryKeySelective(Appointment record);

    int updateByPrimaryKeyWithBLOBs(Appointment record);

    int updateByPrimaryKey(Appointment record);

	void insertAppointmentService(AppointmentEQ appointment);

	List<AppointmentEQ> selectAppointmentSercive(int studentId);

	List<AppointmentEQ> selectAppointment();

	int updateAppointmentState(int id);
}