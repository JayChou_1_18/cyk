package com.zhiyou100.serviceimpl;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhiyou100.mapper.CoachMapper;
import com.zhiyou100.pojo.Coach;
import com.zhiyou100.pojo.CoachExample;
import com.zhiyou100.pojo.CoachExample.Criteria;
import com.zhiyou100.service.CoachService;
@Service
public class CoachServiceImpl implements CoachService {
	
	@Autowired
	private CoachMapper coachMapper;

	@Override
	public String countCoachByMobile(String mobile) {
		int coach = coachMapper.countCoachByMobile(mobile);
		System.out.println("教练+++++++++++++"+coach);
		if(coach>0){
			return "error";
		}
		return "success";
	}

	@Override
	public void insertCoach(Coach coach) {
		coachMapper.insertSelective(coach);
	}

	@Override
	public Coach selectCoach(String mobile) {
		CoachExample example = new CoachExample();
		Criteria createCriteria = example.createCriteria();
		createCriteria.andMobileEqualTo(mobile);
		return 	coachMapper.selectByExample(example).get(0);
		
	}

	@Override
	public void updeatCoachAll(Coach coach) {
		coachMapper.updateByPrimaryKeySelective(coach);
	}

	@Override
	public int countCoachIndex(Coach coach) {
		return coachMapper.countCoachIndex(coach);
		
	}

	@Override
	public Coach selectCoachByMobile(String mobile) {
		return	coachMapper.selectCoachByMobile(mobile);
		
	}

	@Override
	public List<Coach> selectAddCoach() {
		
		return coachMapper.selectAddCoach();
	}

	@Override
	public Coach selectCoachById(Integer id) {
		return coachMapper.selectCoachById(id);
	}
	
	
	
	
	
}
