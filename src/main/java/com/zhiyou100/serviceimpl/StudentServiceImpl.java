package com.zhiyou100.serviceimpl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.fabric.xmlrpc.base.Struct;
import com.zhiyou100.mapper.StudentMapper;
import com.zhiyou100.pojo.Student;
import com.zhiyou100.pojo.StudentExample;
import com.zhiyou100.pojo.StudentExample.Criteria;
import com.zhiyou100.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentMapper studentMapper;

	@Override
	public int selectStudentBymobile(Student student) {
		System.out.println("+++++++++++++++++++++++++++"+student.getMobile());
		int regin = studentMapper.countStudentBymobile(student.getMobile());
		System.out.println("==================================" + regin);
		if (regin == 0) {
			Date date = new Date();
	 		student.setUpdatetime(date);
			studentMapper.insertStudent(student);
			return 1;
		}
		return 0;

	}

	@Override
	public String selectStudentBymobile(String mobile) {
		int regin = studentMapper.countStudentBymobile(mobile);
		if(regin>0){
			return "error";
		}
		return "success";
	}

	@Override
	public int selectStudentLoginIndex(Student student) {
		
		StudentExample example = new StudentExample();
		Criteria criteria = example.createCriteria();
		criteria.andMobileEqualTo(student.getMobile());
		criteria.andPasswordEqualTo(student.getPassword());
		return studentMapper.countByExample1(example);
		
	}

	@Override
	public Student selectStudentByMobile(String mobile) {
		return 	studentMapper.selectStudentByMobile(mobile);
		
	}

	@Override
	public void updeatStudent(Student student) {
		studentMapper.updateByPrimaryKeySelective(student);
	}

}
