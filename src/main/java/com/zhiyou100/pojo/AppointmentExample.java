package com.zhiyou100.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AppointmentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppointmentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andStudentidIsNull() {
            addCriterion("studentId is null");
            return (Criteria) this;
        }

        public Criteria andStudentidIsNotNull() {
            addCriterion("studentId is not null");
            return (Criteria) this;
        }

        public Criteria andStudentidEqualTo(Integer value) {
            addCriterion("studentId =", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidNotEqualTo(Integer value) {
            addCriterion("studentId <>", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidGreaterThan(Integer value) {
            addCriterion("studentId >", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("studentId >=", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidLessThan(Integer value) {
            addCriterion("studentId <", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidLessThanOrEqualTo(Integer value) {
            addCriterion("studentId <=", value, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidIn(List<Integer> values) {
            addCriterion("studentId in", values, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidNotIn(List<Integer> values) {
            addCriterion("studentId not in", values, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidBetween(Integer value1, Integer value2) {
            addCriterion("studentId between", value1, value2, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentidNotBetween(Integer value1, Integer value2) {
            addCriterion("studentId not between", value1, value2, "studentid");
            return (Criteria) this;
        }

        public Criteria andStudentnameIsNull() {
            addCriterion("studentName is null");
            return (Criteria) this;
        }

        public Criteria andStudentnameIsNotNull() {
            addCriterion("studentName is not null");
            return (Criteria) this;
        }

        public Criteria andStudentnameEqualTo(String value) {
            addCriterion("studentName =", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameNotEqualTo(String value) {
            addCriterion("studentName <>", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameGreaterThan(String value) {
            addCriterion("studentName >", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameGreaterThanOrEqualTo(String value) {
            addCriterion("studentName >=", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameLessThan(String value) {
            addCriterion("studentName <", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameLessThanOrEqualTo(String value) {
            addCriterion("studentName <=", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameLike(String value) {
            addCriterion("studentName like", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameNotLike(String value) {
            addCriterion("studentName not like", value, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameIn(List<String> values) {
            addCriterion("studentName in", values, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameNotIn(List<String> values) {
            addCriterion("studentName not in", values, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameBetween(String value1, String value2) {
            addCriterion("studentName between", value1, value2, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentnameNotBetween(String value1, String value2) {
            addCriterion("studentName not between", value1, value2, "studentname");
            return (Criteria) this;
        }

        public Criteria andStudentmobileIsNull() {
            addCriterion("studentMobile is null");
            return (Criteria) this;
        }

        public Criteria andStudentmobileIsNotNull() {
            addCriterion("studentMobile is not null");
            return (Criteria) this;
        }

        public Criteria andStudentmobileEqualTo(String value) {
            addCriterion("studentMobile =", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileNotEqualTo(String value) {
            addCriterion("studentMobile <>", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileGreaterThan(String value) {
            addCriterion("studentMobile >", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileGreaterThanOrEqualTo(String value) {
            addCriterion("studentMobile >=", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileLessThan(String value) {
            addCriterion("studentMobile <", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileLessThanOrEqualTo(String value) {
            addCriterion("studentMobile <=", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileLike(String value) {
            addCriterion("studentMobile like", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileNotLike(String value) {
            addCriterion("studentMobile not like", value, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileIn(List<String> values) {
            addCriterion("studentMobile in", values, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileNotIn(List<String> values) {
            addCriterion("studentMobile not in", values, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileBetween(String value1, String value2) {
            addCriterion("studentMobile between", value1, value2, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andStudentmobileNotBetween(String value1, String value2) {
            addCriterion("studentMobile not between", value1, value2, "studentmobile");
            return (Criteria) this;
        }

        public Criteria andCoachidIsNull() {
            addCriterion("coachId is null");
            return (Criteria) this;
        }

        public Criteria andCoachidIsNotNull() {
            addCriterion("coachId is not null");
            return (Criteria) this;
        }

        public Criteria andCoachidEqualTo(Integer value) {
            addCriterion("coachId =", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidNotEqualTo(Integer value) {
            addCriterion("coachId <>", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidGreaterThan(Integer value) {
            addCriterion("coachId >", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidGreaterThanOrEqualTo(Integer value) {
            addCriterion("coachId >=", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidLessThan(Integer value) {
            addCriterion("coachId <", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidLessThanOrEqualTo(Integer value) {
            addCriterion("coachId <=", value, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidIn(List<Integer> values) {
            addCriterion("coachId in", values, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidNotIn(List<Integer> values) {
            addCriterion("coachId not in", values, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidBetween(Integer value1, Integer value2) {
            addCriterion("coachId between", value1, value2, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachidNotBetween(Integer value1, Integer value2) {
            addCriterion("coachId not between", value1, value2, "coachid");
            return (Criteria) this;
        }

        public Criteria andCoachnameIsNull() {
            addCriterion("coachName is null");
            return (Criteria) this;
        }

        public Criteria andCoachnameIsNotNull() {
            addCriterion("coachName is not null");
            return (Criteria) this;
        }

        public Criteria andCoachnameEqualTo(String value) {
            addCriterion("coachName =", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameNotEqualTo(String value) {
            addCriterion("coachName <>", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameGreaterThan(String value) {
            addCriterion("coachName >", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameGreaterThanOrEqualTo(String value) {
            addCriterion("coachName >=", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameLessThan(String value) {
            addCriterion("coachName <", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameLessThanOrEqualTo(String value) {
            addCriterion("coachName <=", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameLike(String value) {
            addCriterion("coachName like", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameNotLike(String value) {
            addCriterion("coachName not like", value, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameIn(List<String> values) {
            addCriterion("coachName in", values, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameNotIn(List<String> values) {
            addCriterion("coachName not in", values, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameBetween(String value1, String value2) {
            addCriterion("coachName between", value1, value2, "coachname");
            return (Criteria) this;
        }

        public Criteria andCoachnameNotBetween(String value1, String value2) {
            addCriterion("coachName not between", value1, value2, "coachname");
            return (Criteria) this;
        }

        public Criteria andOrderdateIsNull() {
            addCriterion("orderDate is null");
            return (Criteria) this;
        }

        public Criteria andOrderdateIsNotNull() {
            addCriterion("orderDate is not null");
            return (Criteria) this;
        }

        public Criteria andOrderdateEqualTo(Date value) {
            addCriterionForJDBCDate("orderDate =", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("orderDate <>", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateGreaterThan(Date value) {
            addCriterionForJDBCDate("orderDate >", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("orderDate >=", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateLessThan(Date value) {
            addCriterionForJDBCDate("orderDate <", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("orderDate <=", value, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateIn(List<Date> values) {
            addCriterionForJDBCDate("orderDate in", values, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("orderDate not in", values, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("orderDate between", value1, value2, "orderdate");
            return (Criteria) this;
        }

        public Criteria andOrderdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("orderDate not between", value1, value2, "orderdate");
            return (Criteria) this;
        }

        public Criteria andCoachmobileIsNull() {
            addCriterion("coachMobile is null");
            return (Criteria) this;
        }

        public Criteria andCoachmobileIsNotNull() {
            addCriterion("coachMobile is not null");
            return (Criteria) this;
        }

        public Criteria andCoachmobileEqualTo(String value) {
            addCriterion("coachMobile =", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileNotEqualTo(String value) {
            addCriterion("coachMobile <>", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileGreaterThan(String value) {
            addCriterion("coachMobile >", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileGreaterThanOrEqualTo(String value) {
            addCriterion("coachMobile >=", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileLessThan(String value) {
            addCriterion("coachMobile <", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileLessThanOrEqualTo(String value) {
            addCriterion("coachMobile <=", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileLike(String value) {
            addCriterion("coachMobile like", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileNotLike(String value) {
            addCriterion("coachMobile not like", value, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileIn(List<String> values) {
            addCriterion("coachMobile in", values, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileNotIn(List<String> values) {
            addCriterion("coachMobile not in", values, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileBetween(String value1, String value2) {
            addCriterion("coachMobile between", value1, value2, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andCoachmobileNotBetween(String value1, String value2) {
            addCriterion("coachMobile not between", value1, value2, "coachmobile");
            return (Criteria) this;
        }

        public Criteria andTimestatusIsNull() {
            addCriterion("timeStatus is null");
            return (Criteria) this;
        }

        public Criteria andTimestatusIsNotNull() {
            addCriterion("timeStatus is not null");
            return (Criteria) this;
        }

        public Criteria andTimestatusEqualTo(String value) {
            addCriterion("timeStatus =", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusNotEqualTo(String value) {
            addCriterion("timeStatus <>", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusGreaterThan(String value) {
            addCriterion("timeStatus >", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusGreaterThanOrEqualTo(String value) {
            addCriterion("timeStatus >=", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusLessThan(String value) {
            addCriterion("timeStatus <", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusLessThanOrEqualTo(String value) {
            addCriterion("timeStatus <=", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusLike(String value) {
            addCriterion("timeStatus like", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusNotLike(String value) {
            addCriterion("timeStatus not like", value, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusIn(List<String> values) {
            addCriterion("timeStatus in", values, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusNotIn(List<String> values) {
            addCriterion("timeStatus not in", values, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusBetween(String value1, String value2) {
            addCriterion("timeStatus between", value1, value2, "timestatus");
            return (Criteria) this;
        }

        public Criteria andTimestatusNotBetween(String value1, String value2) {
            addCriterion("timeStatus not between", value1, value2, "timestatus");
            return (Criteria) this;
        }

        public Criteria andInserttimeIsNull() {
            addCriterion("insertTime is null");
            return (Criteria) this;
        }

        public Criteria andInserttimeIsNotNull() {
            addCriterion("insertTime is not null");
            return (Criteria) this;
        }

        public Criteria andInserttimeEqualTo(Date value) {
            addCriterion("insertTime =", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotEqualTo(Date value) {
            addCriterion("insertTime <>", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeGreaterThan(Date value) {
            addCriterion("insertTime >", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("insertTime >=", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeLessThan(Date value) {
            addCriterion("insertTime <", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeLessThanOrEqualTo(Date value) {
            addCriterion("insertTime <=", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeIn(List<Date> values) {
            addCriterion("insertTime in", values, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotIn(List<Date> values) {
            addCriterion("insertTime not in", values, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeBetween(Date value1, Date value2) {
            addCriterion("insertTime between", value1, value2, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotBetween(Date value1, Date value2) {
            addCriterion("insertTime not between", value1, value2, "inserttime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNull() {
            addCriterion("updateTime is null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIsNotNull() {
            addCriterion("updateTime is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeEqualTo(Date value) {
            addCriterion("updateTime =", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotEqualTo(Date value) {
            addCriterion("updateTime <>", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThan(Date value) {
            addCriterion("updateTime >", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("updateTime >=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThan(Date value) {
            addCriterion("updateTime <", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeLessThanOrEqualTo(Date value) {
            addCriterion("updateTime <=", value, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeIn(List<Date> values) {
            addCriterion("updateTime in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotIn(List<Date> values) {
            addCriterion("updateTime not in", values, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeBetween(Date value1, Date value2) {
            addCriterion("updateTime between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andUpdatetimeNotBetween(Date value1, Date value2) {
            addCriterion("updateTime not between", value1, value2, "updatetime");
            return (Criteria) this;
        }

        public Criteria andLicenselevelIsNull() {
            addCriterion("licenseLevel is null");
            return (Criteria) this;
        }

        public Criteria andLicenselevelIsNotNull() {
            addCriterion("licenseLevel is not null");
            return (Criteria) this;
        }

        public Criteria andLicenselevelEqualTo(String value) {
            addCriterion("licenseLevel =", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelNotEqualTo(String value) {
            addCriterion("licenseLevel <>", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelGreaterThan(String value) {
            addCriterion("licenseLevel >", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelGreaterThanOrEqualTo(String value) {
            addCriterion("licenseLevel >=", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelLessThan(String value) {
            addCriterion("licenseLevel <", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelLessThanOrEqualTo(String value) {
            addCriterion("licenseLevel <=", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelLike(String value) {
            addCriterion("licenseLevel like", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelNotLike(String value) {
            addCriterion("licenseLevel not like", value, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelIn(List<String> values) {
            addCriterion("licenseLevel in", values, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelNotIn(List<String> values) {
            addCriterion("licenseLevel not in", values, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelBetween(String value1, String value2) {
            addCriterion("licenseLevel between", value1, value2, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andLicenselevelNotBetween(String value1, String value2) {
            addCriterion("licenseLevel not between", value1, value2, "licenselevel");
            return (Criteria) this;
        }

        public Criteria andAccountIsNull() {
            addCriterion("account is null");
            return (Criteria) this;
        }

        public Criteria andAccountIsNotNull() {
            addCriterion("account is not null");
            return (Criteria) this;
        }

        public Criteria andAccountEqualTo(Integer value) {
            addCriterion("account =", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotEqualTo(Integer value) {
            addCriterion("account <>", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThan(Integer value) {
            addCriterion("account >", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("account >=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThan(Integer value) {
            addCriterion("account <", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThanOrEqualTo(Integer value) {
            addCriterion("account <=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountIn(List<Integer> values) {
            addCriterion("account in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotIn(List<Integer> values) {
            addCriterion("account not in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountBetween(Integer value1, Integer value2) {
            addCriterion("account between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("account not between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andOrderstatusIsNull() {
            addCriterion("orderStatus is null");
            return (Criteria) this;
        }

        public Criteria andOrderstatusIsNotNull() {
            addCriterion("orderStatus is not null");
            return (Criteria) this;
        }

        public Criteria andOrderstatusEqualTo(String value) {
            addCriterion("orderStatus =", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusNotEqualTo(String value) {
            addCriterion("orderStatus <>", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusGreaterThan(String value) {
            addCriterion("orderStatus >", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusGreaterThanOrEqualTo(String value) {
            addCriterion("orderStatus >=", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusLessThan(String value) {
            addCriterion("orderStatus <", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusLessThanOrEqualTo(String value) {
            addCriterion("orderStatus <=", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusLike(String value) {
            addCriterion("orderStatus like", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusNotLike(String value) {
            addCriterion("orderStatus not like", value, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusIn(List<String> values) {
            addCriterion("orderStatus in", values, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusNotIn(List<String> values) {
            addCriterion("orderStatus not in", values, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusBetween(String value1, String value2) {
            addCriterion("orderStatus between", value1, value2, "orderstatus");
            return (Criteria) this;
        }

        public Criteria andOrderstatusNotBetween(String value1, String value2) {
            addCriterion("orderStatus not between", value1, value2, "orderstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}