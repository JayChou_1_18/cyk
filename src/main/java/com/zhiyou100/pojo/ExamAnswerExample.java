package com.zhiyou100.pojo;

import java.util.ArrayList;
import java.util.List;

public class ExamAnswerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ExamAnswerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAidIsNull() {
            addCriterion("aid is null");
            return (Criteria) this;
        }

        public Criteria andAidIsNotNull() {
            addCriterion("aid is not null");
            return (Criteria) this;
        }

        public Criteria andAidEqualTo(Integer value) {
            addCriterion("aid =", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotEqualTo(Integer value) {
            addCriterion("aid <>", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidGreaterThan(Integer value) {
            addCriterion("aid >", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidGreaterThanOrEqualTo(Integer value) {
            addCriterion("aid >=", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidLessThan(Integer value) {
            addCriterion("aid <", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidLessThanOrEqualTo(Integer value) {
            addCriterion("aid <=", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidIn(List<Integer> values) {
            addCriterion("aid in", values, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotIn(List<Integer> values) {
            addCriterion("aid not in", values, "aid");
            return (Criteria) this;
        }

        public Criteria andAidBetween(Integer value1, Integer value2) {
            addCriterion("aid between", value1, value2, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotBetween(Integer value1, Integer value2) {
            addCriterion("aid not between", value1, value2, "aid");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNull() {
            addCriterion("topicId is null");
            return (Criteria) this;
        }

        public Criteria andTopicidIsNotNull() {
            addCriterion("topicId is not null");
            return (Criteria) this;
        }

        public Criteria andTopicidEqualTo(Integer value) {
            addCriterion("topicId =", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotEqualTo(Integer value) {
            addCriterion("topicId <>", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThan(Integer value) {
            addCriterion("topicId >", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidGreaterThanOrEqualTo(Integer value) {
            addCriterion("topicId >=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThan(Integer value) {
            addCriterion("topicId <", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidLessThanOrEqualTo(Integer value) {
            addCriterion("topicId <=", value, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidIn(List<Integer> values) {
            addCriterion("topicId in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotIn(List<Integer> values) {
            addCriterion("topicId not in", values, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidBetween(Integer value1, Integer value2) {
            addCriterion("topicId between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andTopicidNotBetween(Integer value1, Integer value2) {
            addCriterion("topicId not between", value1, value2, "topicid");
            return (Criteria) this;
        }

        public Criteria andAsvalueIsNull() {
            addCriterion("asvalue is null");
            return (Criteria) this;
        }

        public Criteria andAsvalueIsNotNull() {
            addCriterion("asvalue is not null");
            return (Criteria) this;
        }

        public Criteria andAsvalueEqualTo(String value) {
            addCriterion("asvalue =", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueNotEqualTo(String value) {
            addCriterion("asvalue <>", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueGreaterThan(String value) {
            addCriterion("asvalue >", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueGreaterThanOrEqualTo(String value) {
            addCriterion("asvalue >=", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueLessThan(String value) {
            addCriterion("asvalue <", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueLessThanOrEqualTo(String value) {
            addCriterion("asvalue <=", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueLike(String value) {
            addCriterion("asvalue like", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueNotLike(String value) {
            addCriterion("asvalue not like", value, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueIn(List<String> values) {
            addCriterion("asvalue in", values, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueNotIn(List<String> values) {
            addCriterion("asvalue not in", values, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueBetween(String value1, String value2) {
            addCriterion("asvalue between", value1, value2, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsvalueNotBetween(String value1, String value2) {
            addCriterion("asvalue not between", value1, value2, "asvalue");
            return (Criteria) this;
        }

        public Criteria andAsinfoIsNull() {
            addCriterion("asinfo is null");
            return (Criteria) this;
        }

        public Criteria andAsinfoIsNotNull() {
            addCriterion("asinfo is not null");
            return (Criteria) this;
        }

        public Criteria andAsinfoEqualTo(String value) {
            addCriterion("asinfo =", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoNotEqualTo(String value) {
            addCriterion("asinfo <>", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoGreaterThan(String value) {
            addCriterion("asinfo >", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoGreaterThanOrEqualTo(String value) {
            addCriterion("asinfo >=", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoLessThan(String value) {
            addCriterion("asinfo <", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoLessThanOrEqualTo(String value) {
            addCriterion("asinfo <=", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoLike(String value) {
            addCriterion("asinfo like", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoNotLike(String value) {
            addCriterion("asinfo not like", value, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoIn(List<String> values) {
            addCriterion("asinfo in", values, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoNotIn(List<String> values) {
            addCriterion("asinfo not in", values, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoBetween(String value1, String value2) {
            addCriterion("asinfo between", value1, value2, "asinfo");
            return (Criteria) this;
        }

        public Criteria andAsinfoNotBetween(String value1, String value2) {
            addCriterion("asinfo not between", value1, value2, "asinfo");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}