package com.zhiyou100.pojo;

import java.util.Date;

public class Appointment {
    private Integer id;

    private Integer studentid;

    private String studentname;

    private String studentmobile;

    private Integer coachid;

    private String coachname;

    private Date orderdate;

    private String coachmobile;

    private String timestatus;

    private Date inserttime;

    private Date updatetime;

    private String licenselevel;

    private Integer account;

    private String orderstatus;

    private String tips;
    
    private String applyMoney; //报名费
    
    private String timeSchedule; //时间
    
    private String addSchedule;//课程
    

    public String getTimeSchedule() {
		return timeSchedule;
	}

	public void setTimeSchedule(String timeSchedule) {
		this.timeSchedule = timeSchedule;
	}

	public String getApplyMoney() {
		return applyMoney;
	}

	public void setApplyMoney(String applyMoney) {
		this.applyMoney = applyMoney;
	}

	public String getAddSchedule() {
		return addSchedule;
	}

	public void setAddSchedule(String addSchedule) {
		this.addSchedule = addSchedule;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentid() {
        return studentid;
    }

    public void setStudentid(Integer studentid) {
        this.studentid = studentid;
    }

    public String getStudentname() {
        return studentname;
    }

    public void setStudentname(String studentname) {
        this.studentname = studentname == null ? null : studentname.trim();
    }

    public String getStudentmobile() {
        return studentmobile;
    }

    public void setStudentmobile(String studentmobile) {
        this.studentmobile = studentmobile == null ? null : studentmobile.trim();
    }

    public Integer getCoachid() {
        return coachid;
    }

    public void setCoachid(Integer coachid) {
        this.coachid = coachid;
    }

    public String getCoachname() {
        return coachname;
    }

    public void setCoachname(String coachname) {
        this.coachname = coachname == null ? null : coachname.trim();
    }

    public Date getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Date orderdate) {
        this.orderdate = orderdate;
    }

    public String getCoachmobile() {
        return coachmobile;
    }

    public void setCoachmobile(String coachmobile) {
        this.coachmobile = coachmobile == null ? null : coachmobile.trim();
    }

    public String getTimestatus() {
        return timestatus;
    }

    public void setTimestatus(String timestatus) {
        this.timestatus = timestatus == null ? null : timestatus.trim();
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getLicenselevel() {
        return licenselevel;
    }

    public void setLicenselevel(String licenselevel) {
        this.licenselevel = licenselevel == null ? null : licenselevel.trim();
    }

    public Integer getAccount() {
        return account;
    }

    public void setAccount(Integer account) {
        this.account = account;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus == null ? null : orderstatus.trim();
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips == null ? null : tips.trim();
    }
}