package com.zhiyou100.pojo;

import java.util.Date;

public class Schedule {
    private Integer id;

    private Integer coachid;

    private Date arrangedate;

    private String timestatus;

    private Date inserttime;

    private Date updatetime;

    private String orderstatus;
    
    private Date parse;

    public Date getParse() {
		return parse;
	}

	public void setParse(Date parse) {
		this.parse = parse;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCoachid() {
        return coachid;
    }

    public void setCoachid(Integer coachid) {
        this.coachid = coachid;
    }

    public Date getArrangedate() {
        return arrangedate;
    }

    public void setArrangedate(Date arrangedate) {
        this.arrangedate = arrangedate;
    }

    public String getTimestatus() {
        return timestatus;
    }

    public void setTimestatus(String timestatus) {
        this.timestatus = timestatus == null ? null : timestatus.trim();
    }

    public Date getInserttime() {
        return inserttime;
    }

    public void setInserttime(Date inserttime) {
        this.inserttime = inserttime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus == null ? null : orderstatus.trim();
    }
}