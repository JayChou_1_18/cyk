package com.zhiyou100.pojo;

public class ExamAnswer {
    private Integer aid;

    private Integer topicid;

    private String asvalue;

    private String asinfo;

    private Integer sort;

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public String getAsvalue() {
        return asvalue;
    }

    public void setAsvalue(String asvalue) {
        this.asvalue = asvalue == null ? null : asvalue.trim();
    }

    public String getAsinfo() {
        return asinfo;
    }

    public void setAsinfo(String asinfo) {
        this.asinfo = asinfo == null ? null : asinfo.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }
}