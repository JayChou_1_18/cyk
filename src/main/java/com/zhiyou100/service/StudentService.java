package com.zhiyou100.service;

import com.zhiyou100.pojo.Student;

public interface StudentService {

	int selectStudentBymobile(Student student);

	String selectStudentBymobile(String mobile);

	int selectStudentLoginIndex(Student student);

	Student selectStudentByMobile(String mobile);

	void updeatStudent(Student student);
	
}
