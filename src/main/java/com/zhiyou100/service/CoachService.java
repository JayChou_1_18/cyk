package com.zhiyou100.service;

import java.util.List;

import com.zhiyou100.pojo.Coach;

public interface CoachService {

	

	String countCoachByMobile(String mobile);

	void insertCoach(Coach coach);

	Coach selectCoach(String mobile);

	void updeatCoachAll(Coach coach);

	int countCoachIndex(Coach coach);

	Coach selectCoachByMobile(String mobile);

	List<Coach> selectAddCoach();

	Coach selectCoachById(Integer id);

	
	
}
